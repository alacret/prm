
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="wf-lato-n4-active wf-active"><script type="text/javascript" async="" src="http://s3.amazonaws.com/ki.js/51730/b0p.js"></script><script>var mpq={track:function(a,b,c){if(typeof c=='function'){window.setTimeout(c,50);}},init:function(){},track_links:function(){},track_forms:function(){}};</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!--3a970064-d3c5-11e3-a757-123140013162 b-->
<title>Proofreading Services | ProofReadingMonkey</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link href="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/reset-81c62fcc415bd2d6fa009d66c47174b6.css" media="screen" rel="stylesheet" type="text/css">
<link href="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/page_defaults-e9f5068d559c53318a0ec6a0d0012fce.css" media="screen" rel="stylesheet" type="text/css">
<style title="page-styles" type="text/css">
  body { color:#333; }
  a {
    color:#3e9ce3;
    text-decoration:none;
  }
  #lp-pom-root {
    display:block;
    background:rgba(255,255,255,1);
    -pie-background:rgba(255,255,255,1);
    margin:auto;
    min-width:1138px;
    behavior:url(/PIE.htc);
  }
  #lp-pom-block-8 {
    display:block;
    background:rgba(44,62,80,1);
    -pie-background:rgba(44,62,80,1);
    border-style:none none none none;
    border-width:undefinedpx;
    margin-left:auto;
    margin-right:auto;
    margin-bottom:0px;
    width:100%;
    height:100px;
    position:relative;
    behavior:url(/PIE.htc);
  }
  #lp-pom-image-354 {
    display:block;
    left:-19px;
    top:-5px;
    z-index:24;
    position:absolute;
  }
  #lp-pom-text-355 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    left:960px;
    top:10px;
    z-index:25;
    width:87px;
    height:26px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-text-356 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    left:942px;
    top:40px;
    z-index:26;
    width:123px;
    height:19px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-text-357 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    left:870px;
    top:66px;
    z-index:27;
    width:268px;
    height:19px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-box-359 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    border-style:none;
    left:319px;
    top:0px;
    z-index:28;
    width:500px;
    height:100px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-block-10 {
    display:block;
    background:rgba(255,255,255,0);
    background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/congruent-pentagon.original.png);
    background-repeat:repeat;
    background-position:center center;
    -pie-background:rgba(255,255,255,0) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/congruent-pentagon.original.png) repeat center center;
    border-style:none none none none;
    border-width:undefinedpx;
    margin-left:auto;
    margin-right:auto;
    margin-bottom:0px;
    width:100%;
    height:625px;
    position:relative;
    behavior:url(/PIE.htc);
  }
  #lp-pom-box-174 {
    display:block;
    background:rgba(248,249,255,1);
    -pie-background:rgba(248,249,255,1);
    border-style:solid;
    border-width:5px;
    border-color:#3e9ce3;
    left:180px;
    top:238px;
    z-index:3;
    width:768px;
    height:360px;
    position:absolute;
    behavior:url(/PIE.htc);
    border-radius:10px;
  }
  #lp-pom-box-174a {
    display: block;
    background: rgba(248,249,255,1);
    -pie-background: rgba(248,249,255,1);
    border-style: solid;
    border-width: 5px;
    border-color: #3e9ce3;
    left: 0px;
    top: 238px;
    z-index: 3;
    width: 310px;
    height: 310px;
    position: absolute;
    behavior: url(/PIE.htc);
    border-radius: 50%;
  }
  #lp-pom-box-174b {
    display: block;
    background: rgba(248,249,255,1);
    -pie-background: rgba(248,249,255,1);
    border-style: solid;
    border-width: 5px;
    border-color: #3e9ce3;
    left: 400px;
    top: 238px;
    z-index: 4;
    width: 310px;
    height: 310px;
    position: absolute;
    behavior: url(/PIE.htc);
    border-radius: 50%;
  }
  #lp-pom-box-174c {
    display: block;
    background: rgba(248,249,255,1);
    -pie-background: rgba(248,249,255,1);
    border-style: solid;
    border-width: 5px;
    border-color: #3e9ce3;
    left: 800px;
    top: 238px;
    z-index: 4;
    width: 310px;
    height: 310px;
    position: absolute;
    behavior: url(/PIE.htc);
    border-radius: 50%;
  }
  #lp-pom-text-288 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    left:232px;
    top:145px;
    z-index:1;
    width:690px;
    height:78px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-block-32 {
    display:block;
    background:rgba(44,62,80,1);
    -pie-background:rgba(44,62,80,1);
    border-style:none none none none;
    border-width:undefinedpx;
    margin:auto;
    width:100%;
    height:69px;
    position:relative;
    behavior:url(/PIE.htc);
  }
  #lp-pom-box-240 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    border-style:solid none none none;
    border-width:1px;
    border-color:#eaeaea;
    left:49px;
    top:724px;
    z-index:2;
    width:834px;
    height:14px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-text-364 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    left:462px;
    top:753px;
    z-index:34;
    width:213px;
    height:14px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-box-365 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    border-style:none;
    left:699px;
    top:736px;
    z-index:35;
    width:199px;
    height:48px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-image-372 {
    display:block;
    left:309px;
    top:744px;
    z-index:42;
    position:absolute;
  }
  #lp-pom-text-373 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    left:9px;
    top:750px;
    z-index:43;
    width:300px;
    height:19px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-text-374 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    left:962px;
    top:753px;
    z-index:44;
    width:123px;
    height:19px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-box-284 {
    display:block;
    background:rgba(255,255,255,0);
    -pie-background:rgba(255,255,255,0);
    border-style:none none solid none;
    border-width:1px;
    border-color:#e9e5e5;
    left:0px;
    top:197px;
    z-index:4;
    width:493px;
    height:5px;
    position:absolute;
    behavior:url(/PIE.htc);
  }
  #lp-pom-button-331a.normal {
    display: block;
    left: 52px;
    top: 100px;
    z-index: 11;
    width: 200px;
    height: 40px;
    position: absolute;
    behavior: url(/PIE.htc);
    border-radius: 5px;
    background-color: #f7941d;
    background: -webkit-linear-gradient(#f7941d,#d75305);
    background: -moz-linear-gradient(#f7941d,#d75305);
    background: -ms-linear-gradient(#f7941d,#d75305);
    background: -o-linear-gradient(#f7941d,#d75305);
    background: linear-gradient(#f7941d,#d75305);
    box-shadow: inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
    text-shadow: 1px 1px #521601;
    -pie-background: linear-gradient(#f7941d,#d75305);
    color: #fff;
    border-style: solid;
    border-width: 1px;
    border-color: #333333;
    font-size: 14px;
    line-height: 19px;
    font-weight: bold;
    font-family: arial;
    text-align: center;
    background-repeat: no-repeat;
  }
  #lp-pom-button-331.normal {
    display:block;
    left:89px;
    top:28px;
    z-index:11;
    width:215px;
    height:40px;
    position:absolute;
    behavior:url(/PIE.htc);
    border-radius:5px;
    background-color:#f7941d;
    background:-webkit-linear-gradient(#f7941d,#d75305);
    background:-moz-linear-gradient(#f7941d,#d75305);
    background:-ms-linear-gradient(#f7941d,#d75305);
    background:-o-linear-gradient(#f7941d,#d75305);
    background:linear-gradient(#f7941d,#d75305);
    box-shadow:inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
    text-shadow:1px 1px #521601;
    -pie-background:linear-gradient(#f7941d,#d75305);
    color:#fff;
    border-style:solid;
    border-width:1px;
    border-color:#333333;
    font-size:16px;
    line-height:19px;
    font-weight:bold;
    font-family:arial;
    text-align:center;
    background-repeat:no-repeat;
  }
  #lp-pom-button-331.uploaded {
   display:block;
   left:89px;
   top:28px;
   z-index:11;
   width:215px;
   height:40px;
   position:absolute;
   behavior:url(/PIE.htc);
   border-radius:5px;
   background-color:#ecf0f1;
   background:-webkit-linear-gradient(#ecf0f1, #c7cbcc);
   background:-moz-linear-gradient(#ecf0f1, #c7cbcc);
   background:-ms-linear-gradient(#ecf0f1, #c7cbcc);
   background:-o-linear-gradient(#ecf0f1, #c7cbcc);
   background:linear-gradient(#ecf0f1, #c7cbcc);
   box-shadow:inset 0px 1px 0px #ffffff, inset 0 -1px 2px #959899;
   text-shadow:1px 1px #24434c;
   color:#fff;
   border-style:solid;
   border-width:1px;
   border-color:#333333;
   font-size:16px;
   line-height:19px;
   font-weight:bold;
   font-family:arial;
   text-align:center;
   background-repeat:no-repeat;
 }
 #lp-pom-text-332 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:319px;
  top:36px;
  z-index:6;
  width:300px;
  height:26px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-332a {
  display: block;
  background: rgba(255,255,255,0);
  -pie-background: rgba(255,255,255,0);
  left: 70px;
  top: 70px;
  z-index: 6;
  width: 300px;
  height: 26px;
  position: absolute;
  behavior: url(/PIE.htc);
}
#lp-pom-text-333 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:-1px;
  top:7px;
  z-index:8;
  width:48px;
  height:42px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-334 {
  display:block;
  background:rgba(107,207,100,1);
  -pie-background:rgba(107,207,100,1);
  border-style:solid;
  border-width:1px;
  border-color:#ccc;
  left:26px;
  top:24px;
  z-index:7;
  width:48px;
  height:48px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:25px;
}
#lp-pom-box-334a {
  display:block;
  background:rgba(107,207,100,1);
  -pie-background:rgba(107,207,100,1);
  border-style:solid;
  border-width:1px;
  border-color:#ccc;
  left:120px;
  top:20px;
  z-index:7;
  width:48px;
  height:48px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:25px;
}
#lp-pom-text-335 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:63px;
  top:101px;
  z-index:9;
  width:447px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-335a {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left: -76px;
  top: 150px;
  z-index:9;
  width:447px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-code-336 {
  display:block;
  left:0px;
  top:156px;
  z-index:10;
  width:521px;
  height:40px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-code-336a {
  display: block;
  left: 0px;
  top: 166px;
  z-index: 10;
  width: 280px;
  height: 55px;
  position: absolute;
  behavior: url(/PIE.htc);
}
#lp-code-336a #words-slider, #time-slider {
  margin-top: 10px;
  margin-left: 25px;
  margin-right: 10px;
  width: 242px;
  display: inline-block;
}

#lp-code-336a #words-value {
  margin-left: 100px;
}
#lp-pom-text-337 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:26px;
  top:134px;
  z-index:11;
  width:367px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-337a {
  display: block;
  background: rgba(255,255,255,0);
  -pie-background: rgba(255,255,255,0);
  left: 47px;
  top: 222px;
  z-index: 11;
  width: 220px;
  height: 22px;
  position: absolute;
  text-align: center;
  behavior: url(/PIE.htc);
}
#lp-pom-box-338 {
  display:block;
  background:rgba(107,207,100,1);
  -pie-background:rgba(107,207,100,1);
  border-style:solid;
  border-width:1px;
  border-color:#ccc;
  left:26px;
  top:220px;
  z-index:12;
  width:48px;
  height:48px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:25px;
}
#lp-pom-box-338b {
  display:block;
  background:rgba(107,207,100,1);
  -pie-background:rgba(107,207,100,1);
  border-style:solid;
  border-width:1px;
  border-color:#ccc;
  left: 130px;
  top: 20px;
  z-index:12;
  width:48px;
  height:48px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:25px;
}
#lp-pom-text-339 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:-1px;
  top:7px;
  z-index:13;
  width:48px;
  height:42px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-340 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:89px;
  top:237px;
  z-index:14;
  width:498px;
  height:16px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-340b {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left: -108px;
  top: 100px;
  z-index:14;
  width:498px;
  height:16px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-code-341 {
  display:block;
  left:0px;
  top:286px;
  z-index:15;
  width:520px;
  height:40px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-code-341b {
  display: block;
  left: -10px;
  top: 145px;
  z-index: 15;
  width: 304px;
  height: 55px;
  position: absolute;
  behavior: url(/PIE.htc);
}
#lp-code-341b #time-slider {
  margin-top: 10px;
  margin-left: 25px;
  margin-right: 10px;
  width: 267px;
  display: inline-block;
}
#lp-code-341b #time-value {
  margin-left: 135px;
}
#lp-pom-box-342 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none solid none none;
  border-width:1px;
  border-color:#e9e5e5;
  left:514px;
  top:20px;
  z-index:16;
  width:6px;
  height:315px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-343 {
  display:block;
  background:rgba(112,150,238,1);
  -pie-background:rgba(112,150,238,1);
  border-style:solid;
  border-width:1px;
  border-color:#ccc;
  left:549px;
  top:74px;
  z-index:17;
  width:193px;
  height:121px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:5px;
}
#lp-pom-box-343c {
  display:block;
  background:rgba(112,150,238,1);
  -pie-background:rgba(112,150,238,1);
  border-style:solid;
  border-width:1px;
  border-color:#ccc;
  left: 52px;
  top: 79px;
  z-index:17;
  width:193px;
  height:121px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:5px;
}
#lp-pom-text-344 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:-1px;
  top:19px;
  z-index:18;
  width:192px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-345 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:-1px;
  top:51px;
  z-index:19;
  width:194px;
  height:77px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-button-346 {
  display:block;
  left:549px;
  top:211px;
  z-index:20;
  width:191px;
  height:40px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:5px;
  background-color:#f7941d;
  background:-webkit-linear-gradient(#f7941d,#d75305);
  background:-moz-linear-gradient(#f7941d,#d75305);
  background:-ms-linear-gradient(#f7941d,#d75305);
  background:-o-linear-gradient(#f7941d,#d75305);
  background:linear-gradient(#f7941d,#d75305);
  box-shadow:inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
  text-shadow:1px 1px #521601;
  -pie-background:linear-gradient(#f7941d,#d75305);
  color:#fff;
  border-style:solid;
  border-width:1px;
  border-color:#333333;
  font-size:16px;
  line-height:19px;
  font-weight:bold;
  font-family:arial;
  text-align:center;
  background-repeat:no-repeat;
}
#lp-pom-button-346c {
  display:block;
  left:54px;
  top:211px;
  z-index:20;
  width:191px;
  height:40px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:5px;
  background-color:#f7941d;
  background:-webkit-linear-gradient(#f7941d,#d75305);
  background:-moz-linear-gradient(#f7941d,#d75305);
  background:-ms-linear-gradient(#f7941d,#d75305);
  background:-o-linear-gradient(#f7941d,#d75305);
  background:linear-gradient(#f7941d,#d75305);
  box-shadow:inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
  text-shadow:1px 1px #521601;
  -pie-background:linear-gradient(#f7941d,#d75305);
  color:#fff;
  border-style:solid;
  border-width:1px;
  border-color:#333333;
  font-size:16px;
  line-height:19px;
  font-weight:bold;
  font-family:arial;
  text-align:center;
  background-repeat:no-repeat;
}
#lp-code-350 {
  display:block;
  left:579px;
  top:270px;
  z-index:21;
  width:135px;
  height:69px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-352 {
  display:block;
  left:421px;
  top:192px;
  z-index:22;
  position:absolute;
}
#lp-pom-image-353 {
  display:block;
  left:26px;
  top:91px;
  z-index:23;
  position:absolute;
}
#lp-pom-text-358 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:21px;
  top:41px;
  z-index:29;
  width:77px;
  height:18px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-360 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:120px;
  top:41px;
  z-index:30;
  width:56px;
  height:18px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-361 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:195px;
  top:41px;
  z-index:31;
  width:110px;
  height:18px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-362 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:324px;
  top:41px;
  z-index:32;
  width:50px;
  height:18px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-363 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:395px;
  top:41px;
  z-index:33;
  width:67px;
  height:18px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-366 {
  display:block;
  left:1px;
  top:11px;
  z-index:36;
  position:absolute;
}
#lp-pom-image-367 {
  display:block;
  left:33px;
  top:11px;
  z-index:37;
  position:absolute;
}
#lp-pom-image-368 {
  display:block;
  left:65px;
  top:11px;
  z-index:38;
  position:absolute;
}
#lp-pom-image-369 {
  display:block;
  left:97px;
  top:11px;
  z-index:39;
  position:absolute;
}
#lp-pom-image-370 {
  display:block;
  left:129px;
  top:11px;
  z-index:41;
  position:absolute;
}
#lp-pom-image-371 {
  display:block;
  left:161px;
  top:11px;
  z-index:40;
  position:absolute;
}
#lp-pom-root .lp-positioned-content {
  width:1138px;
  margin-left:-569px;
}
#lp-pom-block-8 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:100px;
}
#lp-pom-block-10 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:625px;
}
#lp-pom-block-32 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:69px;
}
#lp-pom-button-331.normal:hover {
  background-color:#eb8016;
  background:-webkit-linear-gradient(#eb8016,#cc4504);
  background:-moz-linear-gradient(#eb8016,#cc4504);
  background:-ms-linear-gradient(#eb8016,#cc4504);
  background:-o-linear-gradient(#eb8016,#cc4504);
  background:linear-gradient(#eb8016,#cc4504);
  box-shadow:inset 0px 1px 0px #ffb164,inset 0 -1px 2px #993302;
  -pie-background:linear-gradient(#eb8016,#cc4504);
  color:#fff;
}
#lp-pom-button-331.normal:active {
  background-color:#de7312;
  background:-webkit-linear-gradient(#d75305,#d75305);
  background:-moz-linear-gradient(#d75305,#d75305);
  background:-ms-linear-gradient(#d75305,#d75305);
  background:-o-linear-gradient(#d75305,#d75305);
  background:linear-gradient(#d75305,#d75305);
  box-shadow:inset 0px 2px 4px #5e3007;
  -pie-background:linear-gradient(#d75305,#d75305);
  color:#fff;
}
#lp-pom-button-331.uploaded:hover {
 background-color:#b3dbe5;
 background:-webkit-linear-gradient(#b3dbe5, #97b9c2);
 background:-moz-linear-gradient(#b3dbe5, #97b9c2);
 background:-ms-linear-gradient(#b3dbe5, #97b9c2);
 background:-o-linear-gradient(#b3dbe5, #97b9c2);
 background:linear-gradient(#b3dbe5, #97b9c2);
 box-shadow:inset 0px 1px 0px #fefefe, inset 0 -1px 2px #6f888f;
 color:#fff;
}
#lp-pom-button-331.uploaded:active {
 background-color:#95cbd9;
 background:-webkit-linear-gradient(#c7cbcc, #c7cbcc);
 background:-moz-linear-gradient(#c7cbcc, #c7cbcc);
 background:-ms-linear-gradient(#c7cbcc, #c7cbcc);
 background:-o-linear-gradient(#c7cbcc, #c7cbcc);
 background:linear-gradient(#c7cbcc, #c7cbcc);
 box-shadow:inset 0px 2px 4px #3d5359;
 color:#fff;
}
#lp-pom-button-346:hover {
  background-color:#eb8016;
  background:-webkit-linear-gradient(#eb8016,#cc4504);
  background:-moz-linear-gradient(#eb8016,#cc4504);
  background:-ms-linear-gradient(#eb8016,#cc4504);
  background:-o-linear-gradient(#eb8016,#cc4504);
  background:linear-gradient(#eb8016,#cc4504);
  box-shadow:inset 0px 1px 0px #ffb164,inset 0 -1px 2px #993302;
  -pie-background:linear-gradient(#eb8016,#cc4504);
  color:#fff;
}
#lp-pom-button-346:active {
  background-color:#de7312;
  background:-webkit-linear-gradient(#d75305,#d75305);
  background:-moz-linear-gradient(#d75305,#d75305);
  background:-ms-linear-gradient(#d75305,#d75305);
  background:-o-linear-gradient(#d75305,#d75305);
  background:linear-gradient(#d75305,#d75305);
  box-shadow:inset 0px 2px 4px #5e3007;
  -pie-background:linear-gradient(#d75305,#d75305);
  color:#fff;
}
#lp-pom-button-346c:hover {
  background-color:#eb8016;
  background:-webkit-linear-gradient(#eb8016,#cc4504);
  background:-moz-linear-gradient(#eb8016,#cc4504);
  background:-ms-linear-gradient(#eb8016,#cc4504);
  background:-o-linear-gradient(#eb8016,#cc4504);
  background:linear-gradient(#eb8016,#cc4504);
  box-shadow:inset 0px 1px 0px #ffb164,inset 0 -1px 2px #993302;
  -pie-background:linear-gradient(#eb8016,#cc4504);
  color:#fff;
}
#lp-pom-button-346c:active {
  background-color:#de7312;
  background:-webkit-linear-gradient(#d75305,#d75305);
  background:-moz-linear-gradient(#d75305,#d75305);
  background:-ms-linear-gradient(#d75305,#d75305);
  background:-o-linear-gradient(#d75305,#d75305);
  background:linear-gradient(#d75305,#d75305);
  box-shadow:inset 0px 2px 4px #5e3007;
  -pie-background:linear-gradient(#d75305,#d75305);
  color:#fff;
}
#lp-pom-image-352 .lp-pom-image-container {
  width:137px;
  height:106px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-352 .lp-pom-image-container img {
  width:137px;
  height:106px;
}
#lp-pom-image-353 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-353 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-354 .lp-pom-image-container {
  width:173px;
  height:130px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-354 .lp-pom-image-container img {
  width:173px;
  height:130px;
}
#lp-pom-image-366 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-366 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-367 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-367 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-368 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-368 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-369 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-369 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-370 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-370 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-371 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-371 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-372 .lp-pom-image-container {
  width:122px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-372 .lp-pom-image-container img {
  width:122px;
  height:32px;
}
</style>
<script type="text/javascript" async="" src="http://cdn.mxpnl.com/libs/mixpanel-2.2.min.js"></script><script src="./pricing_files/jquery.min.js" type="text/javascript"></script><style type="text/css"></style>
<script type="text/javascript" src="./pricing_files/unbounce.js"></script><script type="text/javascript">
window.ub.page.id = "3a970064-d3c5-11e3-a757-123140013162";
window.ub.page.variantId = "b";
window.ub.page.name = "pricing";
</script>

<script type="text/javascript">
  var lp = lp || {};
  lp.jQuery = jQuery.noConflict( true );
</script>

<script src="./pricing_files/main.js" type="text/javascript"></script>
<script src="./pricing_files/main(1).js" type="text/javascript"></script>
<style>
  #upload-submit {
    position:absolute;
    left:21px;
    top:190px;
    z-index:3;
    width:236px;
    height:40px;
    behavior:url(/PIE.htc);
    border-radius:5px;
    background-color:#f7941d;
    background:-webkit-linear-gradient(#f7941d,#d75305);
    background:-moz-linear-gradient(#f7941d,#d75305);
    background:-ms-linear-gradient(#f7941d,#d75305);
    background:-o-linear-gradient(#f7941d,#d75305);
    background:linear-gradient(#f7941d,#d75305);
    box-shadow:inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
    text-shadow:1px 1px #521601;
    -pie-background:linear-gradient(#f7941d,#d75305);
    color:#fff;
    border-style:solid;
    border-width:1px;
    border-color:#333333;
    font-size:16px;
    line-height:19px;
    font-weight:bold;
    font-family:arial;
    text-align:center;
    background-repeat:no-repeat;
  }
  #upload-submit:hover {
    background-color:#eb8016;
    background:-webkit-linear-gradient(#eb8016,#cc4504);
    background:-moz-linear-gradient(#eb8016,#cc4504);
    background:-ms-linear-gradient(#eb8016,#cc4504);
    background:-o-linear-gradient(#eb8016,#cc4504);
    background:linear-gradient(#eb8016,#cc4504);
    box-shadow:inset 0px 1px 0px #ffb164,inset 0 -1px 2px #993302;
    -pie-background:linear-gradient(#eb8016,#cc4504);
    color:#fff;
  }
  #upload-submit:active {
    background-color:#de7312;
    background:-webkit-linear-gradient(#d75305,#d75305);
    background:-moz-linear-gradient(#d75305,#d75305);
    background:-ms-linear-gradient(#d75305,#d75305);
    background:-o-linear-gradient(#d75305,#d75305);
    background:linear-gradient(#d75305,#d75305);
    box-shadow:inset 0px 2px 4px #5e3007;
    -pie-background:linear-gradient(#d75305,#d75305);
    color:#fff;
  }
  #uploadform .lp-pom-form-field label {
    font-family:"Lato";
    font-weight:bold;
    font-size:14px;
    line-height:15px;
    color:#000;
  }
  #uploadform .lp-pom-form-field .option label {
    font-family:"Lato";
    font-weight:normal;
    font-size:13px;
    line-height:15px;
    left:18px;
    color:#000;
  }
  #uploadform .lp-pom-form-field .option input { top:2px; }
  #uploadform .lp-pom-form-field input.text {
    box-shadow:inset 0px 2px 3px #dddddd;
    -webkit-box-shadow:inset 0px 2px 3px #dddddd;
    -moz-box-shadow:inset 0px 2px 3px #dddddd;
    border-radius:5px;
  }
  #uploadform .lp-pom-form-field textarea {
    box-shadow:inset 0px 2px 3px #dddddd;
    -webkit-box-shadow:inset 0px 2px 3px #dddddd;
    -moz-box-shadow:inset 0px 2px 3px #dddddd;
    border-style:solid;
    border-width:1px;
    border-color:#bbbbbb;
    border-radius:5px;
  }
  #uploadform .lp-pom-form-field input[type=text] {
    border-style:solid;
    border-width:1px;
    border-color:#bbbbbb;
  }
  #uploadform .lp-pom-form-field select {
    border-style:solid;
    border-width:1px;
    border-color:#bbbbbb;
  }
  #uploadform {
    position:relative;
    margin: auto;
    left:0px;
    top:0px;
    z-index:2;
    width:280px;
    height:205px;
    behavior:url(/PIE.htc);
  }  
</style>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="http://blueimp.github.io/jQuery-File-Upload/css/jquery.fileupload.css">
<link rel="stylesheet" href="http://blueimp.github.io/jQuery-File-Upload/css/jquery.fileupload-ui.css">

<style>
  #words-slider, #time-slider
  {

    margin-top:10px;
    margin-left:25px;
    margin-right:10px;
    width: 360px;
    display:inline-block;
  }
  
  #time-value, #words-value, #words-label
  {
    vertical-align: baseline;
    display:inline-block;
    font-size: 16px;
    font-weight: bold;
  }
  
  #words-value
  {
    width: 60px;
    text-align: center;
  }
  
  .ui-state-default
  {
    background-color:#f7941d !important;
    background:linear-gradient(#f7941d,#d75305) !important;
  }
  
  .ui-state-hover
  {
    background-color:#eb8016 !important;
    background:linear-gradient(#eb8016,#cc4504) !important;
  }
  
  .ui-state-active
  {
    background:linear-gradient(#d75305,#d75305) !important;
    box-shadow:inset 0px 2px 4px #5e3007 !important;
  }
  
  #file-input
  {
    width: 100%;
    height: 100%;
    opacity: 0;
  }
</style>

<style>

  /*Stylesheet by ConversionLab, hello@conversionlab.no*/
  
  /*h1 shadow and weight*/
  
  h1 {

    font-style:normal;
    font-weight:400;
    text-shadow:rgba(0,0,0,0.3) 0 0 5px;
    text-rendering: optimizelegibility;
  }
  
  /*h1 shadow and weight end*/
  
  h2,h3,h4,h5,h6{
    text-rendering: optimizelegibility;
    font-style:normal;
    text-shadow:rgba(0,0,0,0.3) 0 0 5px;
  }


  
  p, ul, div.lp-pom-root .lp-pom-text.nlh p, div.lp-pom-root .lp-pom-text.nlh ul {
    font-family:Lato, 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-style:normal;
    font-weight:normal;
    font-size:15px;
    line-height:23px;
    -webkit-font-smoothing: antialiased;
    text-rendering: optimizelegibility;
  }
  
  
  /*button styles and hover effect end*/
  
  /*full width image block*/
  
  #lp-pom-block-8 {  
    -webkit-background-size: cover !important;
    -moz-background-size: cover !important;   
    -o-background-size: cover !important;
    background-size: cover !important;
  }

  /*full width image block end*/

  /* transparent scroll arrow HAND */
  
  #lp-pom-image-483 { 
    cursor: pointer;
  }
  
  /* transparent scroll arrow HAND end */
  
  /* Menu non-transparent on hover */
  .menu-up {
    opacity:0.7
  }
  
  .menu-up:hover {
    opacity:1;
  }
  /* Menu non-transparent on hover end */
  

</style>
<script src="./pricing_files/webfont.js"></script>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato"><script>
WebFont.load({
  google: {
    families: ['Lato']
  },
  active: function() {
   if(lp && lp.text && typeof lp.text.fixTextHeights === "function")  {
     lp.text.fixTextHeights();
   }
 }
});
</script>
<!--[if IE 7]>
<script>
(function() {
  var sheet = document.styleSheets[1];
  var rules = sheet.rules;
  var rule;
  var index = -1;
  for (var i=0, l=rules.length; i<l; i++){
    rule = rules[i];
    if (rule.selectorText.toLowerCase() == 'div.lp-pom-root .lp-pom-text span') {
      index = i;
      break;
    }
  }
  if (index > -1) {
    sheet.removeRule(index);
    sheet.addRule('div.lp-pom-root .lp-pom-text span', 'line-height:inherit');
  }
})();
</script>
<![endif]-->

<link type="text/css" rel="stylesheet" href="chrome-extension://cpngackimfmofbokmjmljamhdncknpmg/style.css"><script type="text/javascript" charset="utf-8" src="chrome-extension://cpngackimfmofbokmjmljamhdncknpmg/js/page_context.js"></script></head>
<body class="lp-pom-body " screen_capture_injected="true">
  <script src="./pricing_files/jquery-1.11.1.min.js"></script>
  <script src="./pricing_files/jquery-ui.min.js"></script>
  <script src="./pricing_files/jquery.fileupload.js"></script>
  <script src="./pricing_files/jquery.ui.widget.js"></script>
  <script src="./pricing_files/jquery.iframe-transport.js"></script>


  <script>
    var hours = 48;
    var words = 1000;
    var formCompleted = false;
    var price = 20;
    var sliderAlreadyTracked = false;

    var sliderHours = [12, 24, 48, 72, 96, 120, 240, 480, 720];

    var changeUploadingText = 0;
    var processingTime = 0;
    var wordLimit = 125000;
    var gTimeSliderVal = 1;

    /* 
      (rango de palabras)--paso
      0-1000 -- 25
      1000-2500 -- 50
      2500-5000 -- 100
      5000-10000 -- 250
      10000-15000 -- 500
      15000-20000 -- 1000
      20000-50000 -- 2500
      50000-125000 -- 5000
    */
    var words_by_step_range = [25, 50, 100, 250, 500, 1000, 2500, 5000];
    var steps_by_word_range = [40, 30, 25, 20, 10, 5, 12, 15];
    var maxStepWords = 157; // 40+30+25+20+10+5+12+15 = 157 suma de pasos segun segmento
    var realMaxStepWords = 157;

    var maxStepTime = 2;
    var realMaxStepTime = 2;


    /*
    var expRange = [1,2];
    function expF(val){
      return Math.exp(val);
    }
    function baseF(val){
      return Math.log(val);
    }
    function sliderToExp(val, minValueIn, maxValueIn, minValueOut,  maxValueOut){
      minValueIn = minValueIn || 0;
      maxValueIn = maxValueIn  || realMaxStepWords;
      minValueOut = minValueOut || 0;
      maxValueOut = maxValueOut  || wordLimit;
      var distance = Math.abs(expRange[1] - expRange[0]);
      var position = expRange[0]+ (parseFloat(val -minValueIn ))/(maxValueIn - minValueIn) * distance;
      var value =( (expF(position)- expF(expRange[0])) / (expF(expRange[1]) - expF(expRange[0]))  )* (maxValueOut- minValueOut) + minValueOut;
      return parseInt(value/5)*5;
    }
    function expToSlider(exp, minValueIn, maxValueIn, minValueOut,  maxValueOut){
      exp = Math.max(exp,1000);
      minValueIn = minValueIn || 0;
      maxValueIn = maxValueIn  || realMaxStepWords;
      minValueOut = minValueOut || 0;
      maxValueOut = maxValueOut  || wordLimit;
      var distance = Math.abs(expRange[1] - expRange[0]);
      var localExp = (exp - minValueOut) / (maxValueOut - minValueOut) * (expF(expRange[1]) - expF(expRange[0])) + expF(expRange[0]);          
      var position = baseF(localExp);      
      var value = (position - expRange[0])/distance * (maxValueIn - minValueIn) + minValueIn;
      return parseInt(value);
    } */

    function wordsToSlider(total_words) {
      var words_counter = total_words;
      var steps = 0;
      if(words_counter > 50000 && words_counter <= 125000) {
        steps += parseInt((words_counter-50000)/words_by_step_range[7]);
        words_counter = 50000;
      }
      if(words_counter > 20000 && words_counter <= 50000) {
        steps += parseInt((words_counter-20000)/words_by_step_range[6]);
        words_counter = 20000;
      }
      if(words_counter > 15000 && words_counter <= 20000) {
        steps += parseInt((words_counter-15000)/words_by_step_range[5]);
        words_counter = 15000;
      }
      if(words_counter > 10000 && words_counter <= 15000) {
        steps += parseInt((words_counter-10000)/words_by_step_range[4]);
        words_counter = 10000;
      }
      if(words_counter > 5000 && words_counter <= 10000) {
        steps += parseInt((words_counter-5000)/words_by_step_range[3]);
        words_counter = 5000;
      }
      if(words_counter > 2500 && words_counter <= 5000) {
        steps += parseInt((words_counter-2500)/words_by_step_range[2]);
        words_counter = 2500;
      }
      if(words_counter > 1000 && words_counter <= 2500) {
        steps += parseInt((words_counter-1000)/words_by_step_range[1]);
        words_counter = 1000;
      }
      if(words_counter > 0 && words_counter <= 1000) {
        steps += parseInt(words_counter/words_by_step_range[0]);
        words_counter = 0;
      }
      return steps;
    }

    function sliderToWords(total_steps) {
      var steps = total_steps;
      var words_counter = 0;

      if(steps == 157) {
        words_counter = 125000;
      } else {
        if(steps > 142 && steps <= 156 ) {
          words_counter += parseInt((steps-142)*words_by_step_range[7]);
          steps = 142;
        }
        if(steps > 130 && steps <= 142) {
          words_counter += parseInt((steps-130)*words_by_step_range[6]);
          steps = 130;
        }
        if(steps > 125 && steps <= 130) {
          words_counter += parseInt((steps-125)*words_by_step_range[5]);
          steps = 125;
        }
        if(steps > 115 && steps <= 125) {
          words_counter += parseInt((steps-115)*words_by_step_range[4]);
          steps = 115;
        }
        if(steps > 95 && steps <= 115) {
          words_counter += parseInt((steps-95)*words_by_step_range[3]);
          steps = 95;
        }
        if(steps > 70 && steps <= 95) {
          words_counter += parseInt((steps-70)*words_by_step_range[2]);
          steps = 70;
        }
        if(steps > 40 && steps <= 70) {
          words_counter += parseInt((steps-40)*words_by_step_range[1]);
          steps = 40;
        }
        if(steps > 0 && steps <= 40) {
          words_counter += parseInt(steps*words_by_step_range[0]);
          steps = 0;
        }
      }
      return words_counter;
    }

/**

* Min. order is 10$
* Max words is 125,000 words
* There are 3 time options the fastest is 30$ per 1000 words, then 20$ per 1000 words and finally 10$ per 1000 words.
* If there are 1-3000 words in doc the time options are 12-24-48 hours
* If there are 3001-10,000 words in doc the time options are 24-48-72 hours
* If there are 10,001-20,000 words in doc the time options are 48-72-96 hours
* If there are 20,001-30,000 words in doc the time options are 120-240-480 hours
* If there are 30,001 - 125,000 words in doc the time options are 240-480-720 hours

**/

function updateSliderTime(val){  
  if(val === undefined)  
    val = gTimeSliderVal;
  gTimeSliderVal = val;
  words=parseInt(jQuery("#words-value").val());
  if(words>3000 && words <= 10000){
    val +=1;
  }else
  if(words>10000 && words <= 20000){
    val +=2;
  }else
  if(words>20000 && words <= 30000){
    val +=5;
  }else
  if(words>30000 && words <= 125000){ 
    val +=6;
  }
  hours = parseInt(sliderHours[val]);
  var days = 0;
  var time_text = hours + ' hours';
  if(hours >= 24){
    days = parseInt(hours / 24);
    time_text = time_text;
  }
  jQuery("#time-value").text(time_text);
  updatePrice();
}
/* 
    * Min. order is 10$
    * Max words is 125,000 words
    * There are 3 time options the fastest is 30$ per 1000 words, then 20$ per 1000 words and finally 10$ per 1000 words.    
    * If there are 1-3000 words in doc the time options are 12-24-48 hours
    * If there are 3001-10,000 words in doc the time options are 24-48-72 hours
    * If there are 10,001-20,000 words in doc the time options are 48-72-96 hours
    * If there are 20,001-30,000 words in doc the time options are 120-240-480 hours
    * If there are 30,001 - 125,000 words in doc the time options are 240-480-720 hours
    For the last category (30,001 - 125,000 words)-- the price should be 25$ / 15$ / 8$ for 240 / 480 / 720 hours accordingly.
    UPDATED: 0.03, 0.02 and 0.01 / For the last category: 0.025, 0.015 and 0.008
*/
function updatePrice(){
    words = parseInt((jQuery("#words-value").val()+"").replace(/^0+/, ''),10);
    if(isNaN(words)) 
        words = 0;
    //MAx 125000 words
    words = parseInt(Math.min(words,125000));    
    jQuery("#words-value").val(words);
    price = 20;
    //
    var k = 0;
    if(words <= 30000){
      if(gTimeSliderVal === 0)
        k = 0.03;
      else if(gTimeSliderVal === 1)
        k = 0.02;
      else if(gTimeSliderVal === 2)
        k = 0.01;
    
        

    }else{
        if(gTimeSliderVal === 0)
          k = 0.025;
        else if(gTimeSliderVal === 1)
          k = 0.015;
        else if(gTimeSliderVal === 2)
          k = 0.008;
    }
    price = words * k;
//    price = Math.round(price / perThousand) * perThousand;
    price = Math.max(price,10);

    jQuery('#lp-pom-text-345 span span').text('$' + price);
}

     function trackSlider()
     {
      if(sliderAlreadyTracked)
        return false;

      mixpanel.track('slider');
      sliderAlreadyTracked = true;
      return true;
    }

    function updateSliders()
    {
      jQuery("#words-value").val(sliderToWords(jQuery("#words-slider").slider('value')));
      updateSliderTime();

    }

    function shorten(str, maxLength)
    {
      var short = str.substring(0, maxLength);
      if(short.length < str.length)
        return short + '...';
      return str;
    }

    function createSliders(){

      jQuery( "#words-slider" ).slider({
        value:wordsToSlider(words),
        min: 0,
        max: maxStepWords,
        step: 1,
        slide: function( event, ui ) {
          if(formCompleted === true)
            return false;

          trackSlider();
          var value = Math.round(ui.value/maxStepWords* realMaxStepWords);
          words = sliderToWords(value);
          jQuery("#words-value").val(sliderToWords(value));
          updateSliderTime();
        }
      });

      jQuery( "#time-slider" ).slider({
        value:1,
        min: 0,
        max: maxStepTime,
        step: 1,
        slide: function( event, ui ) {
          trackSlider();
          var value = Math.round(ui.value/maxStepTime*realMaxStepTime);
          updateSliderTime(value);
          updatePrice();
        }
      });
    }

    jQuery(function() {

      createSliders();

      jQuery('#words-value').keyup(function() {
        words = jQuery('#words-value').val();
        jQuery( "#words-slider" ).slider('value', wordsToSlider(words));
        updatePrice();
      });

//      jQuery('#lp-pom-button-331').append('<input id="file-input" type="file" name="file" />');
      jQuery('#file-input').fileupload({
        dataType: 'json',
        url: 'count.php',
        fail: function (e, data) {
          alert('We were unable to receive your file. Please try again.');
          clearInterval(changeUploadingText);
          formCompleted = false;
//          jQuery('#lp-pom-button-346').css('opacity', '0.3');
          jQuery('#lp-pom-button-346c').css('opacity', '0.3');
          jQuery('#lp-pom-button-331 span').text('Upload another document');

        },
        start: function(e){
          jQuery('#lp-pom-button-331a').css('opacity', '0.3');
          processingTime = 0;
          jQuery('#lp-pom-button-331a span').text('Uploading...');
          
          changeUploadingText = setInterval(function() {
            var progress = jQuery('#file-input').fileupload('progress');
            var progressPct = (progress.loaded / progress.total * 100).toFixed(0);
            var text = '';

            if(progressPct === 100){
              processingTime++;

              if(processingTime % 10 < 5)
                text = 'Processing...';
              else
                text = 'Still processing...';
            }
            else
              text = 'Uploading... ' + progressPct + '%';

            jQuery('#lp-pom-button-331a span').text(text);
          }, 1000);
          mixpanel.track('file-uploaded');
        },
        done: function(e, data){
          clearInterval(changeUploadingText);
          if(data.result.error){
            alert('We were unable to receive your file. Please upload a .DOC, .DOCX, .PPT, .PPTX, .ODT, .ODP, .RTF or .TXT file');
//            jQuery('#lp-pom-button-331a span').text('Upload another document');
            jQuery('#lp-pom-button-331a span').text('Done!');
          }
          else if(data.result.words > wordLimit){
            alert('Your document contains more than ' + wordLimit + ' words. Please contact us for an individual quote.');
//            jQuery('#lp-pom-button-331a span').text('Upload another document');
            jQuery('#lp-pom-button-331a span').text('Done!');
          }
          else{
              // update sliders
              words = data.result.words;
              jQuery( "#words-slider" ).slider('value', wordsToSlider(words));
              jQuery('#words-value').val(words);
              jQuery('#words-value').prop('disabled', true);
              
              // update labels
              var readableName = data.result.name.substring(32);
              updatePrice();
              jQuery('#lp-pom-text-335 p span').text('You uploaded: ' + shorten(readableName, 20));
//              jQuery('#lp-pom-button-331a span').text('Upload another document');
              jQuery('#lp-pom-button-331a span').text('Done!');
              jQuery('#lp-pom-image-352').show();
              jQuery('#lp-pom-image-353').show();
              jQuery('#lp-pom-text-335 p').css('text-align', 'left');
//              jQuery('#lp-pom-button-331').addClass('uploaded');
//              jQuery('#lp-pom-button-331').removeClass('normal');
              jQuery('#lp-pom-text-337').hide(); // hide "use the slider below .."

              // enable form sending
              jQuery('#hidden-filename').val(data.result.name);
              formCompleted = true;
//              jQuery('#lp-pom-button-346').css('opacity', '1');
              jQuery('#lp-pom-button-346c').css('opacity', '1');
              jQuery('#lp-pom-text-332').hide(); // hide "for an exact price quote"
              jQuery('#words-slider').css('opacity', '0.3'); // disable slider
              updateSliderTime();
            }
//            jQuery('#lp-pom-button-331a').css('opacity', '1');
          }

        });

jQuery('#lp-pom-button-346c').click(function() {   
    console.log("A")
  jQuery('#hidden-price').val(price);
  jQuery('#hidden-words').val(words);
  jQuery('#hidden-hours').val(hours);
  
  if(formCompleted === true)
    jQuery("#next-step-form").submit();
   else  
    alert('Please upload a file before proceeding');

  return false;
});

//jQuery('#lp-pom-button-346').css('opacity', '0.3');
jQuery('#lp-pom-button-346c').css('opacity', '0.3');
jQuery('#lp-pom-image-352').hide();
jQuery('#lp-pom-image-353').hide();
updateSliders();
updatePrice();
});
</script>

<!-- Begin Inspectlet Embed Code -->
<script type="text/javascript" id="inspectletjs">
  window.__insp = window.__insp || [];
  __insp.push(['wid', 557630040]);
  (function() {
    function __ldinsp(){var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }
    if (window.attachEvent){
     window.attachEvent('onload', __ldinsp);
   }else{
     window.addEventListener('load', __ldinsp, false);
   }
 })();
</script>
<!-- End Inspectlet Embed Code -->


<div class="lp-element lp-pom-root" id="lp-pom-root">
  <div class="lp-positioned-content">

    <!-- Form begin -->

    <div class="lp-element lp-pom-box" id="lp-pom-box-174a">
      <div class="lp-element lp-pom-text nlh" id="lp-pom-text-332a" style="height: auto;">
        <p class="lplh-26">
          <span style="font-size:16px;"><strong>For an exact price quote</strong></span>
        </p>
      </div>
      <a class="lp-element lp-pom-button normal" id="lp-pom-button-331a" href="#" target=""><span class="label" style="margin-top: -10px;">Upload a document now</span><input id="file-input" type="file" name="file"></a>
      <div class="lp-element lp-pom-box" id="lp-pom-box-334a">
        <div class="lp-element lp-pom-text nlh" id="lp-pom-text-333" style="height: auto;">
          <p class="lplh-42" style="text-align: center;">
            <strong><span style="color:#ffffff;"><span style="font-size: 26px;">1</span></span></strong>
          </p>
        </div>
      </div>
      <div class="lp-element lp-pom-text nlh" id="lp-pom-text-335a" style="height: auto;">
        <p class="lplh-22" style="text-align: center;">
          <span style="font-size:22px;">OR</span>
        </p>
      </div>
      <div class="lp-element lp-code" id="lp-code-336a">
        <div id="words-slider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a class="ui-slider-handle ui-state-default ui-corner-all" href="http://unbouncepages.com/prm3-pricing/#" style="left: 6.0402684563758395%;"></a></div>
        <input type="text" id="words-value">
        <p id="words-label">words</p>
      </div>
      <div class="lp-element lp-pom-text nlh" id="lp-pom-text-337a" style="height: auto;">
        <p>
          Use the slider above to estimate the number of words</p>
        </div>
      </div>

      <div class="lp-element lp-pom-box" id="lp-pom-box-174b">
        <div class="lp-element lp-pom-box" id="lp-pom-box-338b">
          <div class="lp-element lp-pom-text nlh" id="lp-pom-text-339" style="height: auto;">
            <p class="lplh-42" style="text-align: center;">
              <strong><span style="color:#ffffff;"><span style="font-size: 26px;">2</span></span></strong>
            </p>
          </div>
          <div class="lp-element lp-pom-text nlh" id="lp-pom-text-340b" style="height: auto;">
            <p class="lplh-16">
              <span style="font-size:16px;"><strong>
                      How fast do you need it?
                  </strong></span>
            </p>
          </div>
        </div>
        <div class="lp-element lp-code" id="lp-code-341b">
          <div id="time-slider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a class="ui-slider-handle ui-state-default ui-corner-all" href="http://unbouncepages.com/prm3-pricing/#" style="left: 100%;"></a></div>
          <span id="time-value">12 hours</span>
        </div>
      </div>

      <div class="lp-element lp-pom-box" id="lp-pom-box-174c">
        <div class="lp-element lp-pom-box" id="lp-pom-box-338b">
          <div class="lp-element lp-pom-text nlh" id="lp-pom-text-339" style="height: auto;">
            <p class="lplh-42" style="text-align: center;">
              <strong><span style="color:#ffffff;"><span style="font-size: 26px;">3</span></span></strong>
            </p>
          </div>
        </div>
        <div class="lp-element lp-pom-box" id="lp-pom-box-343c">
          <div class="lp-element lp-pom-text nlh" id="lp-pom-text-344" style="height: auto;">
            <p style="text-align: center;">
              <strong><span style="color:#ffffff;">Your total will be:</span></strong>
            </p>
          </div>
          <div class="lp-element lp-pom-text nlh" id="lp-pom-text-345" style="height: auto;">
            <p class="lplh-77" style="text-align: center;">
              <span style="color:#ffffff;"><span style="font-size:48px;">$10</span></span>
            </p>
          </div>
        </div>
        <a class="lp-element lp-pom-button" id="lp-pom-button-346c" href="order_fcked" target="" style="opacity: 0.3;">
            <span class="label" style="margin-top: -10px;">
                Edit my doc!
            </span>
        </a>
          <div class="lp-element lp-code" id="lp-code-350">
            <form id="next-step-form" action="order" method="POST">
              <input type="hidden" id="hidden-filename" name="hidden-filename">
              <input type="hidden" id="hidden-price" name="hidden-price">
              <input type="hidden" id="hidden-hours" name="hidden-hours">
              <input type="hidden" id="hidden-words" name="hidden-words">
            </form>
          </div>
      </div>

      <!-- <div class="lp-element lp-pom-box" id="lp-pom-box-174">

        <a class="lp-element lp-pom-button normal" id="lp-pom-button-331" href="#" target=""><span class="label" style="margin-top: -10px;">Upload a document now</span><input id="file-input" type="file" name="file"></a>
        <div class="lp-element lp-pom-text nlh" id="lp-pom-text-332" style="height: auto;">
          <p class="lplh-26">
            <span style="font-size:16px;"><strong>for an exact price quote</strong></span>
          </p>
        </div>
        <div class="lp-element lp-pom-box" id="lp-pom-box-334">
          <div class="lp-element lp-pom-text nlh" id="lp-pom-text-333" style="height: auto;">
            <p class="lplh-42" style="text-align: center;">
              <strong><span style="color:#ffffff;"><span style="font-size: 26px;">1</span></span></strong>
            </p>
          </div>
        </div>
        <div class="lp-element lp-pom-text nlh" id="lp-pom-text-335" style="height: auto;">
          <p class="lplh-22" style="text-align: center;">
            <span style="font-size:22px;">OR</span>
          </p>
        </div>
        <div class="lp-element lp-code" id="lp-code-336">
          <div id="words-slider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a class="ui-slider-handle ui-state-default ui-corner-all" href="http://unbouncepages.com/prm3-pricing/#" style="left: 6.0402684563758395%;"></a></div>
          <input type="text" id="words-value">
          <p id="words-label">words</p>
        </div>
        <div class="lp-element lp-pom-text nlh" id="lp-pom-text-337" style="height: auto;">
          <p>
            Use the slider below to estimate the number of words</p>
          </div>
          <div class="lp-element lp-pom-box" id="lp-pom-box-338">
            <div class="lp-element lp-pom-text nlh" id="lp-pom-text-339" style="height: auto;">
              <p class="lplh-42" style="text-align: center;">
                <strong><span style="color:#ffffff;"><span style="font-size: 26px;">2</span></span></strong>
              </p>
            </div>
          </div>
          <div class="lp-element lp-pom-text nlh" id="lp-pom-text-340" style="height: auto;">
            <p class="lplh-16">
              <span style="font-size:16px;"><strong>How soon do you need your paper back?</strong></span>
            </p>
          </div>
          <div class="lp-element lp-code" id="lp-code-341">
            <div id="time-slider" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><a class="ui-slider-handle ui-state-default ui-corner-all" href="http://unbouncepages.com/prm3-pricing/#" style="left: 100%;"></a></div>
            <span id="time-value">48 hours</span>
          </div>
          <div class="lp-element lp-pom-box" id="lp-pom-box-342"></div>
          <div class="lp-element lp-pom-box" id="lp-pom-box-343">
            <div class="lp-element lp-pom-text nlh" id="lp-pom-text-344" style="height: auto;">
              <p style="text-align: center;">
                <strong><span style="color:#ffffff;">Your total will be:</span></strong>
              </p>
            </div>
            <div class="lp-element lp-pom-text nlh" id="lp-pom-text-345" style="height: auto;">
              <p class="lplh-77" style="text-align: center;">
                <span style="color:#ffffff;"><span style="font-size:48px;">$10</span></span>
              </p>
            </div>
          </div>
          <a class="lp-element lp-pom-button" id="lp-pom-button-346" href="order_fcked" target="" style="opacity: 0.3;">
      <span class="label" style="margin-top: -10px;">Go to the last step</span></a>
          <div class="lp-element lp-code" id="lp-code-350">
            <form id="next-step-form" action="order" method="POST">
              <input type="hidden" id="hidden-filename" name="hidden-filename">
              <input type="hidden" id="hidden-price" name="hidden-price">
              <input type="hidden" id="hidden-hours" name="hidden-hours">
              <input type="hidden" id="hidden-words" name="hidden-words">
            </form>
          </div>
          <div class="lp-element lp-pom-image" id="lp-pom-image-352" style="display: none;">
            <div class="lp-pom-image-container" style="overflow: hidden;">
              <img src="./pricing_files/6bg8ii-arrow-red-13.png" alt=""></div>
            </div>
            <div class="lp-element lp-pom-image" id="lp-pom-image-353" style="display: none;">
              <div class="lp-pom-image-container" style="overflow: hidden;">
                <img src="./pricing_files/r0q2cb-document-check-compatibility.png" alt=""></div>
              </div>
            </div> -->


            <div class="lp-element lp-pom-box" id="lp-pom-box-239"></div>
            <div class="lp-element lp-pom-box" id="lp-pom-box-240"></div>

            <!-- Form end -->

            <div class="lp-element lp-pom-text nlh" id="lp-pom-text-288">
              <p class="lplh-77" style="text-align: center;">
                <font color="#2c3e50" face="Lato, sans-serif"><span style="font-size: 54px;"><span style="background-color:#ffff99;">
                            Calculate your expenses
                        </span></span></font>
              </p>
            </div>
            <div class="lp-element lp-pom-image" id="lp-pom-image-354">
              <div class="lp-pom-image-container" style="overflow: hidden;">
                <a href="http://try.proofreadingmonkey.com" target="_self"><img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/t42i4y-proofreadingmonkey-sw-sticker_04t03m04t03m000000.png" alt=""></a>
              </div>
            </div>
            <div class="lp-element lp-pom-text nlh" id="lp-pom-text-355">
              <p class="lplh-26">
                <span style="font-size:16px;"><span style="color:#f7941d;">Questions?</span></span>
              </p>
            </div>
            <div class="lp-element lp-pom-text nlh" id="lp-pom-text-356">
              <p class="lplh-19">
                <span style="font-size:12px;"><span style="letter-spacing: 0.25em;"><span style="font-family:arial,helvetica,sans-serif;"><span style="color:#f7941d;">888-833-8385</span></span></span></span>
              </p>
            </div>
            <div class="lp-element lp-pom-text nlh" id="lp-pom-text-357">
              <p class="lplh-19" style="color: rgb(103, 113, 113); text-align: center;">
                <span style="font-size:10px;"><span style="font-family: arial, helvetica, sans-serif;"><a href="mailto:support@proofreadingmonkey.com" style="text-decoration:none"><span style="color:#ff8c00;">SUPPORT@PROOFREADINGMONKEY.COM</span></a></span></span>
              </p>
            </div>

            <!-- Menu begin -->
<!--
<div class="lp-element lp-pom-box" id="lp-pom-box-359">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-358">
<p class="menu-up lplh-18" style="text-align: center;">
<span style="font-size:14px;"><a href="clkn/http/www.proofreadingmonkey.com/services" style="text-decoration:none; color:#fff;">SERVICES</a></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-360">
<p class="menu-up lplh-18" style="text-align: center;">
<span style="font-size:14px;"><a href="clkn/http/www.proofreadingmonkey.com/about" style="text-decoration:none; color:#fff;">ABOUT</a></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-361">
<p class="menu-up lplh-18" style="text-align: center;">
<span style="font-size:14px;"><a href="clkn/http/www.proofreadingmonkey.com/testimonials" style="text-decoration:none; color:#fff;">TESTIMONIALS</a></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-362">
<p class="menu-up lplh-18" style="text-align: center;">
<span style="font-size:14px;"><a href="clkn/http/www.proofreadingmonkey.com/blog" style="text-decoration:none; color:#fff;">BLOG</a></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-363">
<p class="menu-up lplh-18" style="text-align: center;">
<span style="font-size:14px;"><a href="clkn/http/www.proofreadingmonkey.com/pricing" style="text-decoration:none; color:#fff;">PRICING</a></span>
</p>
</div>
</div>
-->
<!-- Menu end -->

<div class="lp-element lp-pom-text nlh" id="lp-pom-text-364">
  <p class="lplh-14" style="text-align: center;">
    <span style="font-size:9px;"><span style="font-family:lato; letter-spacing: 0.25em;"><span style="color:#bdc3c7;">MADE WITH &hearts; IN NYC</span></span></span>
  </p>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-365">
  <div class="lp-element lp-pom-image" id="lp-pom-image-366">
    <div class="lp-pom-image-container" style="overflow: hidden;">
      <img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/f2isyv-visa.png" alt=""></div>
    </div>
    <div class="lp-element lp-pom-image" id="lp-pom-image-367">
      <div class="lp-pom-image-container" style="overflow: hidden;">
        <img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/4nlrz8-master-card.png" alt=""></div>
      </div>
      <div class="lp-element lp-pom-image" id="lp-pom-image-368">
        <div class="lp-pom-image-container" style="overflow: hidden;">
          <img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/1pim5y6-american-express.png" alt=""></div>
        </div>
        <div class="lp-element lp-pom-image" id="lp-pom-image-369">
          <div class="lp-pom-image-container" style="overflow: hidden;">
            <img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/yx8k80-discover.png" alt=""></div>
          </div>
          <div class="lp-element lp-pom-image" id="lp-pom-image-370">
            <div class="lp-pom-image-container" style="overflow: hidden;">
              <img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/c1pts1-dinersclub.png" alt=""></div>
            </div>
            <div class="lp-element lp-pom-image" id="lp-pom-image-371">
              <div class="lp-pom-image-container" style="overflow: hidden;">
                <img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/ldqz3g-paypal.png" alt=""></div>
              </div>
            </div>
            <div class="lp-element lp-pom-image" id="lp-pom-image-372">
              <div class="lp-pom-image-container" style="overflow: hidden;">
                <img src="//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-pricing/fg8pb0-siteseal-sf-3-h-l-m.png" alt=""></div>
              </div>
              <div class="lp-element lp-pom-text nlh" id="lp-pom-text-373">
                <p class="lplh-19" style="color: rgb(103, 113, 113); text-align: center;">
                  <span style="font-size:10px;"><span style="font-family: arial, helvetica, sans-serif;"><a href="mailto:support@proofreadingmonkey.com" style="text-decoration:none"><span style="color:#ff8c00;">SUPPORT@PROOFREADINGMONKEY.COM</span></a></span></span>
                </p>
              </div>
              <div class="lp-element lp-pom-text nlh" id="lp-pom-text-374">
                <p class="lplh-19">
                  <span style="font-size:12px;"><span style="letter-spacing: 0.25em;"><span style="font-family:arial,helvetica,sans-serif;"><span style="color:#f7941d;">888-833-8385</span></span></span></span>
                </p>
              </div>
            </div>
            <div class="lp-element lp-pom-block" id="lp-pom-block-8">
              <div class="lp-pom-block-content"></div>
            </div>
            <div class="lp-element lp-pom-block" id="lp-pom-block-10">
              <div class="lp-pom-block-content"></div>
            </div>
            <div class="lp-element lp-pom-block" id="lp-pom-block-32">
              <div class="lp-pom-block-content"></div>
            </div>
          </div>
          <script type="text/javascript" language="Javascript">
            var LOCAL=false;var baseserverurl="http://www.uploadthingy.com";var loadimage="<img src=http://49video.resources.s3.amazonaws.com/loading.gif width=16 height=16>";var w_=new Array();w_["Your files are uploading"]="Your files are uploading";w_["via a secure SSL encrypted connection"]="via a secure SSL encrypted connection";w_["Please do not refresh or navigate away from this page until the upload is complete."]="Please do not refresh or navigate away from this page until the upload is complete.";w_["Uploading... "]="Uploading... ";w_["% complete"]="% complete";w_["remaining time"]="remaining time";w_[" minutes"]=" minutes";w_[" seconds"]=" seconds";w_["Ooops. The size of the file(s) you are uploading ("]="Ooops. The size of the file(s) you are uploading (";w_[") is larger than the maximum file size allowed "]=") is larger than the maximum file size allowed ";w_[". <a href=# onclick='authupload();'>Please click here to try again.</a>"]=". <a href=# onclick='authupload();'>Please click here to try again.</a>";w_["Ooops. Your upload has failed."]="Ooops. Your upload has failed. There must have been a problem with the connection somewhere between your computer and the upload server. <a href=# onclick='authupload();'>Please click here to try again.</a> <br><br>Server error: ";function replaceString(D,E){var C="";var B=0;while((il=D.indexOf("<$",B))!=-1){var F=D.indexOf("$>",B);var A=D.substring(il+2,F);if(E[A]){C+=D.substring(B,il)+E[A]}B=F+2}C+=D.substring(B);return C}function setHTML(B,A){if(!document.getElementById(B)){return}document.getElementById(B).innerHTML=A}function d(A){A=unescape(A);return A.replace(/\+/g," ")}function doBRs(A){A=A.replace(/\n/g,"");return A.replace(/<br>/g,"\n")}function $(A){return document.getElementById(A)}function cd(){return document.createElement("div")}function n(A){A=d(A);return A.replace(/ /g,"_")}function empty(A){if(!A){return}while(A.hasChildNodes()){A.removeChild(A.firstChild)}}function trim(A){return A.replace(/^\s+|\s+$/g,"")}var JSC_UPLOADSTATUS=0,JSC_FILELIST=1,JSC_GETSERVERUPLOAD=10,JSC_UPLOADPING=104;var currjson,currpw,curruploadpassword=".none.",currdomain,uploadmetadata,purchasedate,createdate,mmd,md;var sorteduploadlist,uploadcompletehtml=null,uploadcompleteredirect=null,uploadcompletefunction=null,savetozip=false,maxsize=null,showmaxsize=true,sendalertsto;var showblinks=true,trial=false;var oldrefcodes=new Array();var debugstatus="na";function handleJSON(command,json){currjson=json;if(json.s&&json.s=="badcode"){var m="Ooops. The sitecode supplied with this page will not work with this domain. Please make sure you entered the correct domain when you grabbed the code for this uploadthingy at uploadthingy.com.<br><br>Contact us at hello@uploadthingy.com if you have any questions. We'd love to help out!";setHTML("uploadarea",m);return}if(command==JSC_UPLOADSTATUS){if(json.status=="done"||json.error!=null){clearTimeout(uploadchecktimer);var m="";if(json.error){if(json.error=="server"){m="Ooops. Server error uploading file. <a href=# onclick='authupload();'>Please click here to try again</a>."}else{if(json.error=="authcode"){m="Ooops. Invalid upload authorization. <a href=# onclick='authupload();'>Please click here to try again</a>."}else{if(json.error=="video"){m="Ooops. That was not a valid file. Please try again."}else{if(json.error.indexOf("size")==0){var s1="",s2="";if(json.error.indexOf(";")!=-1){s1=json.error.substring(json.error.indexOf(";")+1,json.error.indexOf(">"))+"bytes";s2=json.error.substring(json.error.indexOf(">")+1)+"bytes"}m=w_["Ooops. The size of the file(s) you are uploading ("]+s1+w_[") is larger than the maximum file size allowed "]+(showmaxsize?s2:"")+w_[". <a href=# onclick='authupload();'>Please click here to try again.</a>"]}else{m=w_["Ooops. Your upload has failed."]+json.error}}}}setHTML("uploadarea",m)}else{var uploadrefcode=json.podcastid.substring(json.podcastid.lastIndexOf("$")+1);var nuc="/widget?c="+sitecode+"&a=newupload";if(uploadmetadata!=null){if(uploadmetadata.name!=null){nuc+="&name="+escape(uploadmetadata.name)}if(uploadmetadata.company!=null){nuc+="&company="+escape(uploadmetadata.company)}if(uploadmetadata.description!=null){nuc+="&description="+escape(uploadmetadata.description)}if(uploadmetadata.contact!=null){nuc+="&contact="+escape(uploadmetadata.contact)}if(uploadmetadata.sendtoemail!=null){nuc+="&sendtoemail="+escape(uploadmetadata.sendtoemail)}if(uploadmetadata.sendtotitle!=null){nuc+="&sendtotitle="+escape(uploadmetadata.sendtotitle)}if(savetozip){nuc+="&savetozip=yes"}if(sendalertsto){nuc+="&sendalertsto="+sendalertsto}for(i=0;i<30;i++){if(uploadmetadata["uploadfile"+i]!=null){var fn=uploadmetadata["uploadfile"+i];if(fn.lastIndexOf("\\")!=-1&&fn.lastIndexOf("\\")<fn.length-2){fn=fn.substring(fn.lastIndexOf("\\")+1)}if(fn.lastIndexOf("/")!=-1&&fn.lastIndexOf("/")<fn.length-2){fn=fn.substring(fn.lastIndexOf("/")+1)}nuc+="&uploadfile"+i+"="+escape(fn)}}nuc+="&refcode="+escape(uploadrefcode)+"&authcode="+uploadcode}else{uploadmetadata=new Array()}if(oldrefcodes[uploadrefcode]==null){sendCommand(baseserverurl,nuc);oldrefcodes[uploadrefcode]="sent"}uploadmetadata.refcode=uploadrefcode;var fcnt=0;if(savetozip){uploadmetadata["fhref"+fcnt]=baseserverurl+"/widget?c="+sitecode+"&el="+uploadrefcode+".zip";uploadmetadata["fname"+fcnt]=uploadrefcode+".zip"}else{for(i=0;i<30;i++){if(uploadmetadata["uploadfile"+i]!=null&&uploadmetadata["uploadfile"+i].length>0){var fn=uploadmetadata["uploadfile"+i];var ext="";var di=fn.lastIndexOf(".");if(di!=null){ext=fn.substring(di)}var dl=baseserverurl+"/widget?c="+sitecode+"&el="+uploadrefcode;if(i==0){dl+=ext}else{dl+="!"+i+ext}uploadmetadata["fhref"+fcnt]=dl;uploadmetadata["fname"+fcnt]=fn;fcnt++}}}var fl="";flcnt=0;while(uploadmetadata["fhref"+flcnt]){fl+="<br><a href="+uploadmetadata["fhref"+flcnt]+">"+uploadmetadata["fname"+flcnt]+"</a>";flcnt++}uploadmetadata.filelinks=fl;if(uploadcompleteredirect!=null){uploadcompleteredirect=replaceString(uploadcompleteredirect,uploadmetadata)}if(uploadcompletehtml!=null){m=replaceString(uploadcompletehtml,uploadmetadata)}else{if(uploadcompletefunction!=null){m=uploadcompletefunction()}else{if(uploadcompleteredirect!=null){m="<a href="+uploadcompleteredirect+">Redirecting</a>... "}else{m="Your upload has successfully completed. Use this code to reference your upload: <b>"+uploadrefcode+"</b>.";if(savetozip){m+="<br><br>Here's a link to a zip file with your uploaded files if you'd like to double check:"}else{if(fcnt>1){m+="<br><br>Here are links to your uploaded files if you'd like to double check:"}else{m+="<br><br>Here's a link to your uploaded file if you'd like to double check:"}}m+=uploadmetadata.filelinks;m+="<br><br><a href='javascript:authupload();'>Click here</a> to return to the upload form."}}}if(m){setHTML("uploadarea",m)}submitting=false}submitting=false}else{var info="";var remainingtime=0;if(json.status=="reading"){info+=json.percentcomplete+w_["% complete"];var elapsed=new Date().getTime()-uploadstart;if(eval(json.totalread)>0){remainingtime=elapsed/eval(json.totalread)*(eval(json.contentlength)-eval(json.totalread));remainingtime=remainingtime/1000;if(remainingtime>60){info+=", "+w_["remaining time"]+": "+Math.round(remainingtime/60)+w_[" minutes"]}else{info+=", "+w_["remaining time"]+": "+Math.round(remainingtime)+w_[" seconds"]}}}else{info+=json.status;pendingcnt++;if(pendingcnt>100){clearTimeout(uploadchecktimer);clearTimeout(uploadtimer);if(confirm("Ooops. It seems your upload has failed. Click 'Ok' to start over or 'Cancel' to continue this upload.")){window.location.reload()}pendingcnt=0}}clearTimeout(uploadtimer);var wait=1000;if(isNaN(uploadcnt)){alert("Ooops. It seems your upload has failed. Please contact us at hello@uploadthingy.com if this problem persists");return}if(uploadcnt>50&&remainingtime>20){wait=10000}else{if(uploadcnt>20){wait=5000}else{if(uploadcnt>5){wait=2000}}}debugstatus=json.status+"_"+uploadcnt+"_"+pendingcnt;uploadtimer=setTimeout("upload('go','"+info+"')",wait)}}else{if(command==JSC_GETSERVERUPLOAD){if(json.s=="ok"||json.s=="trialover"){if(!json.pd&&Date.parse(json.cd)>Date.parse("6/28/2008")&&Math.round((new Date().getTime()-Date.parse(json.cd))/(24*60*60*1000))>8){if(confirm("Ooops. Your trial period has ended or your subscription has been canceled.\n\nClick Ok to redirect to the subscription purchase page at uploadthingy.com. Click Cancel to remain on this page.")){if(window.parent){window.parent.location="http://www.uploadthingy.com/purchase.html"}else{document.location="http://www.uploadthingy.com/purchase.html"}}return}uploadcode=json.c;videoserverurl=json.u;if(videoserverurl.indexOf("https:")==0&&baseserverurl.indexOf("https:")==-1){baseserverurl="https"+baseserverurl.substring(4)}submitting=true;document.getElementById("uploadform").action=json.u+"/cire?uploadcode="+json.c+(maxsize==null?"":"&maxsize="+maxsize);if(document.getElementById("uploadid")){document.getElementById("uploadid").value="widget$"+json.d+"$"+json.c.substring(0,json.c.indexOf("_"))}else{document.getElementById("id").value="widget$"+json.d+"$"+json.c.substring(0,json.c.indexOf("_"))}document.getElementById("authcode").value=uploadcode;document.getElementById("uploadform").submit();uploadcnt=0;pendingcnt=0;uploadstart=new Date().getTime();uploadtimer=setTimeout("upload('go')",1000);uploadchecktimer=setTimeout("uploadCheck()",30000);uploadcntcheck=0}else{if(json.s=="notactive"){setHTML("uploadarea","Ooops. This uploadthingy has been deactivated.")}else{if(json.s=="noauth"){curruploadpassword=prompt("Please enter your upload password: ","");if(curruploadpassword!=null&&curruploadpassword.length>0){sendCommand(baseserverurl,"/widget?c="+sitecode+"&a=authupload&up="+curruploadpassword,null)}else{curruploadpassword=".none."}}else{if(confirm("Ooops. Could not connect to server. Click ok to try again.")){submitting=false;upload("start")}}}}}else{if(command==JSC_UPLOADPING){if(uploadcompleteredirect!=null){if(window.parent){window.parent.location=uploadcompleteredirect}else{document.location=uploadcompleteredirect}}}else{setHTML("uploadarea","Ooops. There was an error: "+json.s+", command="+command+". Please contact us at hello@uploadthingy.com.")}}}}var commandcnt=Math.ceil(Math.random()*10000000),ccbase=new Date().getTime()-1231877000000;var showwaitsave;function sendCommand(A,D,C){if(C){showwaitsave=document.getElementById(C).innerHTML;setHTML(C,"<p>"+loadimage+" &nbsp;Loading...</p>")}var B=document.createElement("script");B.src=A+D+"&cnt="+ccbase+"."+commandcnt;empty(document.getElementById("dataarea"));document.getElementById("dataarea").appendChild(B);commandcnt++}var uploadtimer,uploadcnt,uploadcode,videoserverurl,pendingcnt;var uploadchecktimer,uploadcntcheck;var uploadstart,submitting=false;function upload(C,B){if(C=="start"&&!submitting){sendCommand(baseserverurl,"/widget?c="+sitecode+"&a=authupload&up="+curruploadpassword,null);return false}else{++uploadcnt;var A="<p class=49widget_p>"+w_["Your files are uploading"];if(videoserverurl.indexOf("https:")==0){A+=" <b>"+w_["via a secure SSL encrypted connection"]+"</b>"}A+=". "+w_["Please do not refresh or navigate away from this page until the upload is complete."]+"</p>";A+="<p>"+loadimage+" &nbsp;"+w_["Uploading... "];if(B){A+=B}A+="</p>";setHTML("uploadarea",A);sendCommand(videoserverurl,"/jsc?uc="+uploadcode+"&debug="+encodeURIComponent(debugstatus),null)}return true}function authupload(){if(LOCAL){baseserverurl="http://localhost"}addIFrameToDOM("uploadtarget");addElementToDOM("span","dataarea");var A=new Array();A.c="xx_xx";A.d="x";var B="<a name=widget_top></a><form method='post' action='";B+="' enctype='multipart/form-data' id='uploadform' name='uploadform' target='uploadtarget' onsubmit=\"if (checkform()) {return upload('start');} else return false;\" >";B+=getUploadForm(A);B+="<input type=hidden name='suploadvideo' value='Upload'>";if(savetozip){B+="<input type=hidden name='savetozip' value='yes'>"}B+="</form><br>";setHTML("uploadarea",B)}var checkerrors=0;function uploadCheck(){if(isNaN(uploadcnt)){alert("Ooops. It seems your upload has failed. Please contact us at hello@uploadthingy.com if this problem persists");return}if(uploadcntcheck==uploadcnt){if(++checkerrors<5){upload("go","Reconnecting...")}else{if(confirm("Ooops. It seems your upload has failed. Click 'Ok' to start over or 'Cancel' to continue this upload.")){window.location.reload()}else{upload("go","Reconnecting...")}}}uploadcntcheck=uploadcnt;uploadchecktimer=setTimeout("uploadCheck()",30000)}function addIFrameToDOM(B){if(document.getElementById(B)){return}var A=document.getElementsByTagName("body").item(0);bgl=document.createElement("div");bgl.setAttribute("id","ut_ifc"+B);bgl.setAttribute("name","ut_ifc"+B);bgl.style.display="none";bgl.style.width=0;bgl.style.height=0;bgl.style.border=0;bgl.innerHTML="<iframe id='"+B+"' name='"+B+"' src='' style='width:0px;height:0px;border:0;'></iframe>";A.insertBefore(bgl,A.firstChild)}function addElementToDOM(C,D){if(!document.getElementById(D)){var A=document.getElementsByTagName("body").item(0);var B=document.createElement(C);B.setAttribute("id",D);B.setAttribute("name",D);B.style.display="none";B.style.width=0;B.style.height=0;B.style.border=0;A.insertBefore(B,A.firstChild)}};
          </script>
          <!-- Qualaroo for proofreadingmonkey.com -->
          <!-- Paste this code right after the <body> tag on every page of your site. -->
          <script type="text/javascript">
            var _kiq = _kiq || [];
            (function(){
              setTimeout(function(){
                var d = document, f = d.getElementsByTagName('script')[0], s = d.createElement('script'); s.type = 'text/javascript';
                s.async = true; s.src = '//s3.amazonaws.com/ki.js/51730/b0p.js'; f.parentNode.insertBefore(s, f);
              }, 1);
            })();
          </script>
          <!-- start Mixpanel --><script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
            for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f)}})(document,window.mixpanel||[]);
              mixpanel.init("5cd765de35906b506851dd3455d5d4f7");</script><!-- end Mixpanel -->
          </body>
          </html>
