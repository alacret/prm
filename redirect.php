<?php

ob_start();
require 'phpmailer/PHPMailerAutoload.php';
require 'db.php';

function shorten($str, $len)
{
	$short = substr($str, 0, $len);
	
	if(strlen($short) < strlen($str))
		return $short . '...';
	return $str;
}

function create_mail()
{
	$mail = new PHPMailer(true);
	$mail->isSMTP();
	$mail->Host = 'relay-hosting.secureserver.net'; 
	//$mail->SMTPAuth = true;
	$mail->Username = 'support@proofreadingmonkey.com'; 
	$mail->Password = 'Lc2eAjI4PNkr';
	//$mail->SMTPSecure = 'ssl';
	$mail->SetFrom('support@proofreadingmonkey.com', 'Proofreading Monkey');
	return $mail;
}

if(!isset($_POST['filename']))
{
	header('Location: index.php');
	die();
}

$originalFilename = shorten(substr($_POST['filename'], 32), 25);

try
{
	$db = new DB('orders');

	$db->add(array(
		':date' => strtotime('now'),
		':email' => $_POST['uploader_contact'],
		':file' => $_POST['filename'],
		':words' => $_POST['words'],
		':total' => $_POST['price'],
		':time' => $_POST['hours'],
		':payment' => $_POST['payment_method']
	));

	$mail = create_mail();

	$mail->addAddress('burchenya@gmail.com');
	$mail->addAttachment(dirname(__FILE__) . '/uploads/' . $_POST['filename'], $originalFilename);
	$mail->Subject = sprintf('[PRM] %sh %s', $_POST['hours'], $_POST['uploader_contact']);
	$mail->Body = "$_POST[uploader_contact] has purchased proofreading of $_POST[words] words for $_POST[price] deadline is $_POST[hours] hours";
	$mail->send();
	
	$body = file_get_contents('client_mail_template.html');
	$replace = array(
		'{subject}' => 'Order confirmation',
		'{filename}' => $originalFilename,
		'{hours}' => $_POST['hours'],
		'{total}' => $_POST['price']
	);
	
	$body = str_replace(array_keys($replace), array_values($replace), $body);	
	
	$mail = create_mail();
	$mail->IsHTML(true);
	$mail->addAddress($_POST['uploader_contact']);
	$mail->Subject = 'Order confirmation';
	$mail->Body = $body;
	$mail->send();
	
	if($_POST['payment_method'] == 'Paypal')
		header('Location: https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=burchenya@gmail.com&currency_code=USD&item_name=Proofreading%20services&amount=' . $_POST['price']);
	else if($_POST['payment_method'] == 'Credit Card')
		header('Location: https://www.2checkout.com/checkout/purchase?sid=2107485&mode=2CO&li_0_type=product&li_0_name=Proofreading%20services&li_0_price=' . $_POST['price']);
}
catch (phpmailerException $e)
{
	echo 'An error occured: ' . $e->getMessage();
}