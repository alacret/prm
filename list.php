<?php

//$password = '1brkwp5bss';
date_default_timezone_set('Europe/Riga');

include 'db.php';
$db = new DB('orders');

if(isset($_GET['action']) && $_GET['action'] == 'complete')
	$db->complete($_GET['id']);

$data = $db->get(strtotime('-1 week'));

?>

<html>
<head>
<style>
table {
	text-align: center;
	width: 1200px;
	margin: auto;
}
th td {
	font-weight: bold;
}
</style>
</head>
<body>

<table>
	<tr>
		<th width="5%">Completed?</th>
		<th width="15%">Date</th>
		<th width="20%">E-mail</th>
		<th width="5%">File</th>
		<th width="10%">Words</th>
		<th width="10%">Total</th>
		<th width="10%">Time</th>
		<th width="15%">Payment method</th>
		<th width="10%">Actions</th>
	</tr>
	
	<?php foreach($data as $order): ?>
	<tr>
		<td><?php echo $order['completed'] ? 'Yes' : 'No'; ?></td>
		<td><?php echo strftime("%b %d %H:%M:%S", $order['date']); ?></td>
		<td><?php echo htmlentities($order['email']); ?></td>
		<td><a href="<?php echo 'uploads/'.urlencode($order['file']); ?>">get</a></td>
		<td><?php echo htmlentities($order['words']); ?></td>
		<td><?php echo htmlentities('$'.$order['total']); ?></td>
		<td><?php echo htmlentities($order['time'].'h'); ?></td>
		<td><?php echo htmlentities($order['payment']); ?></td>
		<td><a href="<?php echo "list.php?action=complete&id=$order[id]"; ?>">Mark complete</a></td>
	</tr>
	<?php endforeach; ?>
</table>
</body>
</html>