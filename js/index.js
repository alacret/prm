var sitecode = "cHJvb2ZyZWFkaW5nbW9ua2V5LmNvbQ==";
      authupload();
                           
    function allowSending()
    {
      document.prm_sending = true;
      doUpload();
    }
  
    function getUploadForm(json) {
        showblinks = false;
        var h = "";
      

        h += "<input type='hidden' id='authcode' name='authcode' value='" + json.c + "'>";
        h += "<input type='hidden' id='uploadid' name='id' value='widget$" + json.d + "$" + json.c.substring(0, json.c.indexOf("_")) + "'>";
        h += "<input type='hidden' id='callbackurl' name='callbackurl' value='" + baseserverurl + "'>";
        h += "<input type='hidden' name='savemeta' value='yes'>";
        h += '<fieldset class="clearfix" style="width: 280px; height: 205px;" id="FIELDSET_152">';
        h += '<div class="lp-pom-form-field clearfix" style="width: 280px; height: 51px; top: 0px;" id="DIV_153">';
        h += '<label for="file" class="main" style="top: 0px; width: auto; height: auto;" id="LABEL_154">My Document*</label><input type="file" size="40" id="uploadfile0" name="file0" style="top: 19px; left: 0px; width: 262px; font-size: 15px; height: 32px; padding-left: 8px; padding-right: 8px; lineheight: 15px; margin-top: 20px">';
        h += '</div>';
        h += '<div class="lp-pom-form-field clearfix" style="width: 280px; height: 51px; top: 60px;" id="DIV_156">';
        h += '<label for="uploader_contact" class="main" style="top: 0px; width: auto; height: auto;" id="LABEL_157">Email*</label><input type="text" id="uploader_contact" name="uploader_contact" class="text valid" style="top: 19px; left: 0px; width: 262px; font-size: 15px; height: 32px; padding-left: 8px; padding-right: 8px; lineheight: 15px;">';
        h += '</div>';
    h += '<div class="lp-pom-form-field clearfix" style="width: 280px; height: 51px; top: 120px;" id="DIV_159">';
        h += '<label for="uploader_description" class="main" style="top: 0px; width: auto; height: auto;" id="LABEL_160">Number of Words*</label><input type="text" id="uploader_description" name="uploader_description" class="text" style="top: 19px; left: 0px; width: 262px; font-size: 15px; height: 32px; padding-left: 8px; padding-right: 8px; lineheight: 15px;" onkeyup="updateText();">';
        h += '</div>';
    h += '<div style="width: 249px; height: 61px; top: 180px;" class="lp-pom-form-field clearfix" id="DIV_162">';
     h += '<label style="top: 0px; width: auto;" class="main" for="payment_method" id="LABEL_163">Payment method *</label>';
     h += '<div style="top: 25px; left: 0px; width: 249px;" class="optionsList" id="DIV_164">';
     h += '<div class="option" id="DIV_165">';
     h += '<input type="radio" name="payment_method" id="payment_method" value="Paypal"><label for="payment_method_Paypal" id="Label_167">Paypal</label>';
     h += '</div>';
     h += '<div class="option" id="DIV_168">';
     h += '<input type="radio" name="payment_method" id="payment_method" value="Credit Card"><label for="payment_method_Credit Card" id="Label_170">Credit Card</label>';
     h += '</div>';
     h += '</div>';
     h += '</div>';
        h += '</fieldset>';
      h += '<a class="lp-element lp-pom-button" id="upload-submit" onClick="doUpload();" href="#" style="top: 260px"><span class="label" id="SPAN_172" style="margin-top: -10px;">Proofread my document!</span></a><div id="tracking" style="display:none;"></div>';
     
        return h;
    }

    function doUpload() {
        if (checkform()) {
            upload('start');
        }
    }

    function updateText()
    {
      var count = document.getElementById('uploader_description').value;
      var validation = new RegExp('^\\s*\\d+\\s*$'); // 1+ digits with whitespace at the beginning/end
      var text = 'We will proofread your document for just 10$ per 1000 words';
      
      if(validation.test(count) && !isNaN(parseFloat(count)))
      {
           var cost = (count * 0.01);
           text = 'We will proofread your document for just ' + cost.toFixed(2) + '$';
      }
      
      jQuery('#SPAN_112').text(text);
    }
  
    function checkform() {
        var found = false;
        var cnt = 0;
        while (document.getElementById('uploadfile' + cnt)) {
            if (document.getElementById('uploadfile' + cnt).value.length > 0) {
                found = true;
                break;
            }
            cnt++;
        }
        if (!found) {
            alert('Please select a file to upload.');
            return false;
        }
      
        var chunks = document.getElementById('uploadfile0').value.split('.');
        var extension = chunks[chunks.length - 1];
      
        if(extension != 'doc' && extension != 'docx' && extension != 'lyx')
        {
            alert('Please upload a Microsoft Word file - .doc or .docx format.');
            return false;
        }
        
      
        var count = document.getElementById('uploader_description').value;
        var validation = new RegExp('^\\s*\\d+\\s*$'); // 1+ digits with whitespace at the beginning/end
      
        if(!validation.test(count) || isNaN(parseFloat(count)))
        {
             alert('Please enter correct number of words. Use only digits, no comma or spaces.');
             return false;
        }
        
        if(document.getElementById('uploader_contact').value.trim().length == 0)
        {
             alert('Please enter an e-mail address, where your proofread document will be sent.');
             return false;
        }


        
    if(document.getElementById('payment_method').value.trim().length == 0)
        {
             alert('Please select a payment method');
             return false;
        }
      
        var price = (parseFloat(count) * 0.01);        
      
        // Validation is complete
        // First form submit loads AdWords conversion tracking and sets a timeout for the second formsubmit
        // Second (automatic) form submit actually sends the form contents
        if(document.prm_sending == true)
        {
          uploadmetadata = new Array();
          uploadmetadata['name'] = 'N/A'
          uploadmetadata['contact'] = document.getElementById('uploader_contact').value;
          uploadmetadata['description'] = 'Words: ' + document.getElementById('uploader_description').value;
          uploadmetadata['uploadfile0'] = document.getElementById('uploadfile0').value;
          // Emulate unbounce native button click
          //document.getElementById('lp-pom-button-389').click();
          //alert(jQuery('input:radio[name="payment_method"]:checked').val());
          if(jQuery('input:radio[name="payment_method"]:checked').val() == "Paypal"){
          uploadcompleteredirect = 'https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=burchenya@gmail.com&currency_code=USD&item_name=Proofreading%20services';
          uploadcompleteredirect += '&amount=' + price.toFixed(2);
      }
      if(jQuery('input:radio[name="payment_method"]:checked').val() == "Credit Card"){
          uploadcompleteredirect = 'https://www.2checkout.com/checkout/purchase?sid=2107485&mode=2CO&li_0_type=product&li_0_name=Proofreading%20services';
          uploadcompleteredirect += '&li_0_price=' + price;
          }
          return true;
        }
        else
        {
          // Avoid duplicate submits
          if(document.prm_clicked != true)
          {
            // Insert tracking image
            // We no longer use iframe because it's simplier to pass conversion value this way
            document.getElementById('tracking').innerHTML = '<iframe src="http://www.proofreadingmonkey.com/tracking/" style="border:none;width:1px;height:1px;" frameborder="0" marginwidth="0" marginheight="0"></iframe>';
            
           // document.getElementById('tracking').innerHTML = '<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/988359818/?label=Yd-vCIawjAcQitmk1wM&guid=ON&script=0&value=' + /*price*/0 + '"/>';
            document.prm_clicked = true;
            setTimeout(allowSending, 1000);
          }
          
          return false;
        }
      
      //return true;
    }

