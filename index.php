<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0038)http://unbouncepages.com/prm3-landing/ -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

      <!--1cb70bf8-d3ba-11e3-b390-22000ac464fa a-->
<title>Proofreading Services | ProofreadingMonkey.com</title>
<meta name="keywords" content="proofreading service">
<meta name="description" content="Professional proofreading and editing services. Two editors work on every document. We find your errors before other people do.">
<link href="./index_files/reset-81c62fcc415bd2d6fa009d66c47174b6.css" media="screen" rel="stylesheet" type="text/css">
<link href="./index_files/page_defaults-4c9db0d0b46ddf4baece4f0b99da6dcd.css" media="screen" rel="stylesheet" type="text/css">
<style title="page-styles" type="text/css">
body { color:#333; }
a {
  color:#3e9ce3;
  text-decoration:none;
}
#lp-pom-text-33 {
  left:0px;
  top:2403px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  z-index:5;
  width:251px;
  height:17px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-38 {
  left:526px;
  top:737px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none;
  z-index:13;
  width:412px;
  height:88px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-54 {
  left:526px;
  top:630px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none;
  z-index:17;
  width:407px;
  height:88px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-59 {
  left:10px;
  top:738px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none;
  z-index:9;
  width:399px;
  height:107px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-112 {
  left:844px;
  top:2403px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  z-index:6;
  width:96px;
  height:17px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-root {
  display:block;
  background:rgba(255,255,255,1);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png);
  background-repeat:repeat;
  background-position:center center;
  -pie-background:rgba(255,255,255,1) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png) repeat center center;
  margin:auto;
  min-width:1200px;
  behavior:url(/PIE.htc);
}
#lp-pom-block-8 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none none none none;
  border-width:undefinedpx;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:1px;
  width:100%;
  height:123px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-text-224 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:574px;
  top:14px;
  z-index:7;
  width:419px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-239 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:solid none none none;
  border-width:1px;
  border-color:#eaeaea;
  left:62px;
  top:123px;
  z-index:8;
  width:834px;
  height:14px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-267 {
  display:block;
  left:0px;
  top:0px;
  z-index:21;
  position:absolute;
}
#lp-pom-image-268 {
  display:block;
  left:52px;
  top:25px;
  z-index:22;
  position:absolute;
}
#lp-pom-block-225 {
  display:block;
  background:rgba(255,255,255,1);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png);
  background-repeat:repeat;
  background-position:center center;
  -pie-background:rgba(255,255,255,1) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png) repeat center center;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:932px;
  height:401px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-text-286 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:8px;
  top:252px;
  z-index:50;
  width:74px;
  height:116px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-287 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:353px;
  top:252px;
  z-index:51;
  width:89px;
  height:116px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-288 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:41px;
  top:238px;
  z-index:49;
  width:345px;
  height:114px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-289 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:-21px;
  top:363px;
  z-index:48;
  width:470px;
  height:26px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-video-349 {
  display:block;
  left:434px;
  top:208px;
  z-index:31;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-button-399 {
  display:block;
  left:102px;
  top:427px;
  z-index:52;
  width:262px;
  height:40px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:5px;
  background-color:#f7941d;
  background:-webkit-linear-gradient(#f7941d,#d75305);
  background:-moz-linear-gradient(#f7941d,#d75305);
  background:-ms-linear-gradient(#f7941d,#d75305);
  background:-o-linear-gradient(#f7941d,#d75305);
  background:linear-gradient(#f7941d,#d75305);
  box-shadow:inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
  text-shadow:1px 1px #521601;
  -pie-background:linear-gradient(#f7941d,#d75305);
  color:#fff;
  border-style:solid;
  border-width:1px;
  border-color:#333333;
  font-size:16px;
  line-height:19px;
  font-weight:bold;
  font-family:arial;
  text-align:center;
  background-repeat:no-repeat;
}
#lp-pom-text-403 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:0px;
  top:137px;
  z-index:53;
  width:932px;
  height:115px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-404 {
  display:block;
  left:-17px;
  top:389px;
  z-index:58;
  position:absolute;
}
#lp-pom-text-405 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:84px;
  top:476px;
  z-index:59;
  width:300px;
  height:19px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-block-366 {
  display:block;
  background:rgba(255,255,255,0);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/bgservices.original.jpg);
  background-repeat:repeat;
  background-position:center center;
  -pie-background:rgba(255,255,255,0) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/bgservices.original.jpg) repeat center center;
  border-style:solid none none none;
  border-width:1px;
  border-color:#54514a;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:91px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-text-367 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:141px;
  top:550px;
  z-index:33;
  width:650px;
  height:84px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-block-26 {
  display:block;
  background:rgba(255,255,255,0);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/bgservices.original.jpg);
  background-repeat:repeat;
  background-position:center center;
  -pie-background:rgba(255,255,255,0) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/bgservices.original.jpg) repeat center center;
  border-style:none none solid none;
  border-width:1px;
  border-color:#54514a;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:69px;
  width:100%;
  height:261px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-box-358 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none;
  left:0px;
  top:630px;
  z-index:1;
  width:413px;
  height:100px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-block-307 {
  display:block;
  background:rgba(255,255,255,1);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png);
  background-repeat:no-repeat;
  background-position:center center;
  -pie-background:rgba(255,255,255,1) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png) no-repeat center center;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:68px;
  width:932px;
  height:377px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-code-334 {
  display:block;
  left:-45px;
  top:884px;
  z-index:30;
  width:1022px;
  height:505px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-35 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:43px;
  top:4px;
  z-index:14;
  width:201px;
  height:25px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-36 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:43px;
  top:29px;
  z-index:15;
  width:340px;
  height:60px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-55 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:43px;
  top:4px;
  z-index:18;
  width:201px;
  height:25px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-56 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:43px;
  top:29px;
  z-index:19;
  width:313px;
  height:60px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-60 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:43px;
  top:4px;
  z-index:10;
  width:201px;
  height:25px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-61 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:43px;
  top:29px;
  z-index:11;
  width:328px;
  height:80px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-block-368 {
  display:block;
  background:rgba(253,253,251,1);
  -pie-background:rgba(253,253,251,1);
  border-style:solid none none none;
  border-width:1px;
  border-color:#6F6353;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:560px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-image-370 {
  display:block;
  left:186px;
  top:1450px;
  z-index:34;
  position:absolute;
}
#lp-pom-text-371 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:384px;
  top:1434px;
  z-index:35;
  width:133px;
  height:84px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-372 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:67px;
  top:1518px;
  z-index:36;
  width:300px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-373 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:578px;
  top:1518px;
  z-index:37;
  width:300px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-374 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:578px;
  top:1637px;
  z-index:38;
  width:348px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-375 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:578px;
  top:1759px;
  z-index:39;
  width:300px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-376 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:67px;
  top:1807px;
  z-index:40;
  width:300px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-377 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:67px;
  top:1719px;
  z-index:41;
  width:300px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-381 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:578px;
  top:1540px;
  z-index:42;
  width:300px;
  height:88px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-382 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:67px;
  top:1741px;
  z-index:43;
  width:300px;
  height:66px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-383 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:67px;
  top:1831px;
  z-index:44;
  width:300px;
  height:66px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-384 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:578px;
  top:1787px;
  z-index:45;
  width:305px;
  height:110px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-385 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:578px;
  top:1665px;
  z-index:46;
  width:315px;
  height:88px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-386 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:67px;
  top:1540px;
  z-index:47;
  width:300px;
  height:44px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-410 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:dashed;
  border-width:1px;
  border-color:#3e9ce3;
  left:67px;
  top:1595px;
  z-index:54;
  width:427px;
  height:115px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-block-360 {
  display:block;
  background:rgba(255,255,255,1);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/testimonialbg.original.jpg);
  background-repeat:repeat;
  background-position:center center;
  -pie-background:rgba(255,255,255,1) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/testimonialbg.original.jpg) repeat center center;
  border-style:solid none solid none;
  border-width:1px;
  border-color:#6F6353;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:27px;
  width:100%;
  height:394px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-code-363 {
  display:block;
  left:-200px;
  top:1956px;
  z-index:32;
  width:1265px;
  height:393px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-block-32 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:67px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-box-276 {
  display:block;
  background:rgba(255,255,255,1);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png);
  background-repeat:repeat;
  background-position:center center;
  -pie-background:rgba(255,255,255,1) url(//d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/prm3-landing/white-wall-hash-2x.original.png) repeat center center;
  border-style:none;
  left:368px;
  top:2382px;
  z-index:23;
  width:309px;
  height:59px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-178 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:50px;
  top:32px;
  z-index:2;
  width:337px;
  height:66px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-179 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:50px;
  top:5px;
  z-index:4;
  width:300px;
  height:25px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-250 {
  display:block;
  left:0px;
  top:6px;
  z-index:20;
  position:absolute;
}
#lp-pom-image-251 {
  display:block;
  left:0px;
  top:7px;
  z-index:16;
  position:absolute;
}
#lp-pom-image-252 {
  display:block;
  left:0px;
  top:6px;
  z-index:12;
  position:absolute;
}
#lp-pom-image-254 {
  display:block;
  left:10px;
  top:5px;
  z-index:3;
  position:absolute;
}
#lp-pom-image-270 {
  display:block;
  left:49px;
  top:14px;
  z-index:24;
  position:absolute;
}
#lp-pom-image-271 {
  display:block;
  left:121px;
  top:14px;
  z-index:26;
  position:absolute;
}
#lp-pom-image-272 {
  display:block;
  left:85px;
  top:14px;
  z-index:25;
  position:absolute;
}
#lp-pom-image-273 {
  display:block;
  left:193px;
  top:14px;
  z-index:28;
  position:absolute;
}
#lp-pom-image-274 {
  display:block;
  left:229px;
  top:14px;
  z-index:29;
  position:absolute;
}
#lp-pom-image-275 {
  display:block;
  left:157px;
  top:14px;
  z-index:27;
  position:absolute;
}
#lp-pom-text-411 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none none none none;
  border-width:undefinedpx;
  left:-1px;
  top:14px;
  z-index:56;
  width:503px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-412 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:none none none solid;
  border-width:1px;
  border-color:#000000;
  left:8px;
  top:6px;
  z-index:57;
  width:3px;
  height:46px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-413 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:none none none solid;
  border-width:1px;
  border-color:#000000;
  left:8px;
  top:65px;
  z-index:55;
  width:3px;
  height:46px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-root .lp-positioned-content {
  width:932px;
  margin-left:-466px;
}
#lp-pom-image-250 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-250 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-251 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-251 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-252 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-252 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-254 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-254 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-267 .lp-pom-image-container {
  width:90px;
  height:115px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-267 .lp-pom-image-container img {
  width:90px;
  height:115px;
}
#lp-pom-image-268 .lp-pom-image-container {
  width:417px;
  height:78px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-268 .lp-pom-image-container img {
  width:417px;
  height:78px;
}
#lp-pom-image-270 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-270 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-271 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-271 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-272 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-272 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-273 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-273 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-274 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-274 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-275 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-275 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-370 .lp-pom-image-container {
  width:519px;
  height:7px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-370 .lp-pom-image-container img {
  width:519px;
  height:7px;
}
#lp-pom-button-399:hover {
  background-color:#eb8016;
  background:-webkit-linear-gradient(#eb8016,#cc4504);
  background:-moz-linear-gradient(#eb8016,#cc4504);
  background:-ms-linear-gradient(#eb8016,#cc4504);
  background:-o-linear-gradient(#eb8016,#cc4504);
  background:linear-gradient(#eb8016,#cc4504);
  box-shadow:inset 0px 1px 0px #ffb164,inset 0 -1px 2px #993302;
  -pie-background:linear-gradient(#eb8016,#cc4504);
  color:#fff;
}
#lp-pom-button-399:active {
  background-color:#de7312;
  background:-webkit-linear-gradient(#d75305,#d75305);
  background:-moz-linear-gradient(#d75305,#d75305);
  background:-ms-linear-gradient(#d75305,#d75305);
  background:-o-linear-gradient(#d75305,#d75305);
  background:linear-gradient(#d75305,#d75305);
  box-shadow:inset 0px 2px 4px #5e3007;
  -pie-background:linear-gradient(#d75305,#d75305);
  color:#fff;
}
#lp-pom-image-404 .lp-pom-image-container {
  width:123px;
  height:95px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-404 .lp-pom-image-container img {
  width:123px;
  height:95px;
}
</style>
<script src="./index_files/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="./index_files/unbounce.js"></script><script type="text/javascript">
window.ub.page.id = "1cb70bf8-d3ba-11e3-b390-22000ac464fa";
window.ub.page.variantId = "a";
window.ub.page.name = "landing";
</script>

<script type="text/javascript">
var lp = lp || {};
lp.jQuery = jQuery.noConflict( true );
</script>

<script src="./index_files/main.js" type="text/javascript"></script>
<script src="./index_files/main(1).js" type="text/javascript"></script>
<style>
#upload-submit {
  position:absolute;
  left:21px;
  top:190px;
  z-index:3;
  width:236px;
  height:40px;
  behavior:url(/PIE.htc);
  border-radius:5px;
  background-color:#f7941d;
  background:-webkit-linear-gradient(#f7941d,#d75305);
  background:-moz-linear-gradient(#f7941d,#d75305);
  background:-ms-linear-gradient(#f7941d,#d75305);
  background:-o-linear-gradient(#f7941d,#d75305);
  background:linear-gradient(#f7941d,#d75305);
  box-shadow:inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
  text-shadow:1px 1px #521601;
  -pie-background:linear-gradient(#f7941d,#d75305);
  color:#fff;
  border-style:solid;
  border-width:1px;
  border-color:#333333;
  font-size:16px;
  line-height:19px;
  font-weight:bold;
  font-family:arial;
  text-align:center;
  background-repeat:no-repeat;
}
#upload-submit:hover {
  background-color:#eb8016;
  background:-webkit-linear-gradient(#eb8016,#cc4504);
  background:-moz-linear-gradient(#eb8016,#cc4504);
  background:-ms-linear-gradient(#eb8016,#cc4504);
  background:-o-linear-gradient(#eb8016,#cc4504);
  background:linear-gradient(#eb8016,#cc4504);
  box-shadow:inset 0px 1px 0px #ffb164,inset 0 -1px 2px #993302;
  -pie-background:linear-gradient(#eb8016,#cc4504);
  color:#fff;
}
#upload-submit:active {
  background-color:#de7312;
  background:-webkit-linear-gradient(#d75305,#d75305);
  background:-moz-linear-gradient(#d75305,#d75305);
  background:-ms-linear-gradient(#d75305,#d75305);
  background:-o-linear-gradient(#d75305,#d75305);
  background:linear-gradient(#d75305,#d75305);
  box-shadow:inset 0px 2px 4px #5e3007;
  -pie-background:linear-gradient(#d75305,#d75305);
  color:#fff;
}
#uploadform .lp-pom-form-field label {
  font-family:"Lato";
  font-weight:bold;
  font-size:14px;
  line-height:15px;
  color:#000;
}
#uploadform .lp-pom-form-field .option label {
  font-family:"Lato";
  font-weight:normal;
  font-size:13px;
  line-height:15px;
  left:18px;
  color:#000;
}
#uploadform .lp-pom-form-field .option input { top:2px; }
#uploadform .lp-pom-form-field input.text {
  box-shadow:inset 0px 2px 3px #dddddd;
  -webkit-box-shadow:inset 0px 2px 3px #dddddd;
  -moz-box-shadow:inset 0px 2px 3px #dddddd;
  border-radius:5px;
}
#uploadform .lp-pom-form-field textarea {
  box-shadow:inset 0px 2px 3px #dddddd;
  -webkit-box-shadow:inset 0px 2px 3px #dddddd;
  -moz-box-shadow:inset 0px 2px 3px #dddddd;
  border-style:solid;
  border-width:1px;
  border-color:#bbbbbb;
  border-radius:5px;
}
#uploadform .lp-pom-form-field input[type=text] {
  border-style:solid;
  border-width:1px;
  border-color:#bbbbbb;
}
#uploadform .lp-pom-form-field select {
  border-style:solid;
  border-width:1px;
  border-color:#bbbbbb;
}
#uploadform {
  position:relative;
  margin: auto;
  left:0px;
  top:0px;
  z-index:2;
  width:280px;
  height:205px;
  behavior:url(/PIE.htc);
}
</style>
<script src="./index_files/jquery.min.js" type="text/javascript"></script>
<style type="text/css">
  .countdownHolder{
    width:450px;
    margin:0 auto;
	font: 40px/1.5 'Open Sans Condensed',sans-serif;
	text-align:center;
	letter-spacing:-3px;
}

.position{
	display: inline-block;
	height: 1.6em;
	overflow: hidden;
	position: relative;
	width: 1.05em;
}

.digit{
	position:absolute;
	display:block;
	width:1em;
	background-color:#444;
	border-radius:0.2em;
	text-align:center;
	color:#fff;
	letter-spacing:-1px;
}

.digit.static{
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.35);
	
	background-image: linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -o-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -moz-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -webkit-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	background-image: -ms-linear-gradient(bottom, #3A3A3A 50%, #444444 50%);
	
	background-image: -webkit-gradient(
		linear,
		left bottom,
		left top,
		color-stop(0.5, #3A3A3A),
		color-stop(0.5, #444444)
	);
}

/**
 * You can use these classes to hide parts
 * of the countdown that you don't need.
 */

.countDays{  display:none !important; }
.countDiv0{  display:none !important; }
.countHours{}
.countDiv1{}
.countMinutes{}
.countDiv2{}
.countSeconds{}


.countDiv{
	display:inline-block;
	width:16px;
	height:1.6em;
	position:relative;
}

.countDiv:before,
.countDiv:after{
	position:absolute;
	width:5px;
	height:5px;
	background-color:#444;
	border-radius:50%;
	left:50%;
	margin-left:-3px;
	top:0.5em;
	box-shadow:1px 1px 1px rgba(4, 4, 4, 0.5);
	content:'';
}

.countDiv:after{
	top:0.9em;
}
</style>
<style>
#testimonial {
border-radius: 15px;
}
</style>
<style>
#H2_1 {
    color: rgb(85, 85, 85);
    height: 31px;
    position: relative;
    text-align: center;
    text-decoration: none solid rgb(85, 85, 85);
    white-space: nowrap;
    width: 711px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 1px solid rgb(204, 204, 204);
    font: normal normal normal 21px/31px MuseoSlab-300, Georgia, serif;
    margin: 25px 38.5px 35px;
    outline: rgb(85, 85, 85) none 0px;
    padding: 10px 0px;
}/*#H2_1*/

#H2_1:after {
    background-position: -28px 0px;
    box-shadow: rgb(204, 204, 204) 2px 0px 2px 0px inset;
    color: rgb(85, 85, 85);
    height: 47px;
    position: absolute;
    right: -27px;
    text-align: center;
    text-decoration: none solid rgb(85, 85, 85);
    top: 2px;
    white-space: nowrap;
    width: 26px;
    content: '';
    background: rgba(0, 0, 0, 0) url(http://d2nt7j7ljjsiah.cloudfront.net/assets/v2_backgrounds/bg-white-ribbon-corners-5048454a398c7fe19a5de8fc0fa42d5f.png) no-repeat scroll -28px 0px / auto padding-box border-box;
    border: 0px none rgb(85, 85, 85);
    font: normal normal normal 21px/31px MuseoSlab-300, Georgia, serif;
    margin: 0px;
    outline: rgb(85, 85, 85) none 0px;
}/*#H2_1:after*/

#H2_1:before {
    background-position: 0px 0px;
    box-shadow: rgb(204, 204, 204) -2px 0px 2px 0px inset;
    color: rgb(85, 85, 85);
    height: 47px;
    left: -27px;
    position: absolute;
    text-align: center;
    text-decoration: none solid rgb(85, 85, 85);
    top: 2px;
    white-space: nowrap;
    width: 26px;
    content: '';
    background: rgba(0, 0, 0, 0) url(http://d2nt7j7ljjsiah.cloudfront.net/assets/v2_backgrounds/bg-white-ribbon-corners-5048454a398c7fe19a5de8fc0fa42d5f.png) no-repeat scroll 0px 0px / auto padding-box border-box;
    border: 0px none rgb(85, 85, 85);
    font: normal normal normal 21px/31px MuseoSlab-300, Georgia, serif;
    margin: 0px;
    outline: rgb(85, 85, 85) none 0px;
}/*#H2_1:before*/

#B_2 {
    color: rgb(85, 85, 85);
    text-align: center;
    text-decoration: none solid rgb(85, 85, 85);
    white-space: nowrap;
    border: 0px none rgb(85, 85, 85);
    font: normal normal bold 21px/31px MuseoSlab-300, Georgia, serif;
    outline: rgb(85, 85, 85) none 0px;
}/*#B_2*/
</style>

<style>

strong {
    font-weight: bold; 
  }
  
#DIV_2 {
    color: rgb(34, 34, 34);
    height: 305px;
    position: relative;
    text-decoration: none solid rgb(34, 34, 34);
    width: 921px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    margin: 0px 233px;
    outline: rgb(34, 34, 34) none 0px;
}/*#DIV_2*/

#DIV_3 {
    color: rgb(34, 34, 34);
    float: left;
    height: 199px;
    text-decoration: none solid rgb(34, 34, 34);
    width: 261px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    margin: 62px 67px 0px 0px;
    outline: rgb(34, 34, 34) none 0px;
}/*#DIV_3*/

#DIV_4 {
    color: rgb(34, 34, 34);
    height: 154px;
    position: relative;
    text-decoration: none solid rgb(34, 34, 34);
    width: 225px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 3px solid rgb(111, 99, 83);
    border-radius: 5px 5px 5px 5px;
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    outline: rgb(34, 34, 34) none 0px;
    padding: 17px 15px;
}/*#DIV_4*/
  
#DIV_20 {
    color: rgb(34, 34, 34);
    height: 132px;
    position: relative;
    text-decoration: none solid rgb(34, 34, 34);
    width: 225px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 3px solid rgb(111, 99, 83);
    border-radius: 5px 5px 5px 5px;
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    outline: rgb(34, 34, 34) none 0px;
    padding: 17px 15px;
}/*#DIV_20*/

#P_5, #P_21 {
    color: rgb(111, 99, 83);
    height: 88px;
    text-decoration: none solid rgb(111, 99, 83);
    width: 225px;
    border: 0px none rgb(111, 99, 83);
    font: normal normal normal 16px/22px Lato, sans-serif;
    margin: 0px;
    outline: rgb(111, 99, 83) none 0px;
}/*#P_5, #P_21*/

#SPAN_6, #SPAN_14, #SPAN_22 {
    background-position: 50% 0%;
    bottom: -12px;
    color: rgb(34, 34, 34);
    display: block;
    height: 12px;
    left: 112.5px;
    position: absolute;
    text-decoration: none solid rgb(34, 34, 34);
    width: 30px;
    background: rgba(0, 0, 0, 0) url(http://www.theorydesign.ca/img/bubble-arrow.png) no-repeat scroll 50% 0% / auto padding-box border-box;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    margin: 0px 0px 0px -15px;
    outline: rgb(34, 34, 34) none 0px;
}/*#SPAN_6, #SPAN_14, #SPAN_22*/

#DIV_7, #DIV_15, #DIV_23 {
    color: rgb(34, 34, 34);
    height: 50px;
    text-decoration: none solid rgb(34, 34, 34);
    width: 216px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    outline: rgb(34, 34, 34) none 0px;
    padding: 20px 0px 0px 45px;
}/*#DIV_7, #DIV_15, #DIV_23*/

#IMG_8 {
    color: rgb(34, 34, 34);
    display: block;
    float: left;
    height: 51px;
    text-decoration: none solid rgb(34, 34, 34);
    vertical-align: middle;
    width: 49px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    margin: 0px 13px 0px 0px;
    outline: rgb(34, 34, 34) none 0px;
    padding-bottom:100px;
}/*#IMG_8*/

#P_9, #P_17, #P_25 {
    color: rgb(111, 99, 83);
    height: 22px;
    text-decoration: none solid rgb(111, 99, 83);
    width: 216px;
    border: 0px none rgb(111, 99, 83);
    font: normal normal bold 14px/22px Lato;
    margin: 0px;
    outline: rgb(111, 99, 83) none 0px;
    padding: 6px 0px 0px;
}/*#P_9, #P_17, #P_25*/

#SPAN_10, #SPAN_18, #SPAN_26 {
    color: rgb(111, 99, 83);
    text-decoration: none solid rgb(111, 99, 83);
    border: 0px none rgb(111, 99, 83);
    font: normal normal normal 14px/22.399999618530273px Lato, sans-serif;
    outline: rgb(111, 99, 83) none 0px;
}/*#SPAN_10, #SPAN_18, #SPAN_26*/

#DIV_11 {
    color: rgb(34, 34, 34);
    float: left;
    height: 243px;
    text-decoration: none solid rgb(34, 34, 34);
    width: 261px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    margin: 62px 67px 0px 0px;
    outline: rgb(34, 34, 34) none 0px;
}/*#DIV_11*/

#DIV_12 {
    color: rgb(34, 34, 34);
    height: 154px;
    position: relative;
    text-decoration: none solid rgb(34, 34, 34);
    width: 225px;
    background: rgb(255, 255, 255) none repeat scroll 0% 0% / auto padding-box border-box;
    border: 3px solid rgb(111, 99, 83);
    border-radius: 5px 5px 5px 5px;
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    outline: rgb(34, 34, 34) none 0px;
    padding: 17px 15px;
}/*#DIV_12*/

#P_13 {
    color: rgb(111, 99, 83);
    height: 132px;
    text-decoration: none solid rgb(111, 99, 83);
    width: 225px;
    border: 0px none rgb(111, 99, 83);
    font: normal normal normal 16px/22px Lato, sans-serif;
    margin: 0px;
    outline: rgb(111, 99, 83) none 0px;
}/*#P_13*/

#IMG_16, #IMG_24 {
    color: rgb(34, 34, 34);
    display: block;
    float: left;
    height: 51px;
    text-decoration: none solid rgb(34, 34, 34);
    vertical-align: middle;
    width: 50px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    margin: 0px 13px 0px 0px;
    outline: rgb(34, 34, 34) none 0px;
    padding-bottom:100px;
}/*#IMG_16, #IMG_24*/

#DIV_19 {
    color: rgb(34, 34, 34);
    float: left;
    height: 199px;
    text-decoration: none solid rgb(34, 34, 34);
    width: 261px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    margin: 62px 0px 0px;
    outline: rgb(34, 34, 34) none 0px;
}/*#DIV_19*/

#DIV_27 {
    color: rgb(34, 34, 34);
    height: 305px;
    text-decoration: none solid rgb(34, 34, 34);
    width: 921px;
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    outline: rgb(34, 34, 34) none 0px;
}/*#DIV_27*/

#DIV_27:after {
    clear: both;
    color: rgb(34, 34, 34);
    display: table;
    text-decoration: none solid rgb(34, 34, 34);
    width: 1px;
    content: '';
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    outline: rgb(34, 34, 34) none 0px;
}/*#DIV_27:after*/

#DIV_27:before {
    color: rgb(34, 34, 34);
    display: table;
    text-decoration: none solid rgb(34, 34, 34);
    width: 1px;
    content: '';
    border: 0px none rgb(34, 34, 34);
    font: normal normal normal 16px/22.399999618530273px Lato, sans-serif;
    outline: rgb(34, 34, 34) none 0px;
}/*#DIV_27:before*/
</style>
<style>
  #lp-pom-text-453{background:rgba(255,255,255,0);
-pie-background:rgba(255,255,255,0);
position:absolute;
left:14px;
top:-13px;
z-index:72;
width:412px;
height:39px;
behavior:url(/PIE.htc)}#lp-pom-box-454{background:rgba(255,255,255,0);
-pie-background:rgba(255,255,255,0);
border-style:none none none solid;
border-width:1px;
border-color:#000;
position:absolute;
left:7px;
top:6px;
z-index:74;
width:99px;
height:42px;
behavior:url(/PIE.htc)}#lp-pom-box-455{background:rgba(255,255,255,0);
-pie-background:rgba(255,255,255,0);
border-style:none none none solid;
border-width:1px;
border-color:#000;
position:absolute;
left:7px;
top:67px;
z-index:75;
width:99px;
height:39px;
behavior:url(/PIE.htc)}#lp-pom-text-456{background:rgba(255,255,255,0);
-pie-background:rgba(255,255,255,0);
position:absolute;
left:14px;
top:47px;
z-index:73;
width:412px;
height:39px;
behavior:url(/PIE.htc)}#lp-pom-box-452{background:rgba(255,255,255,1);
-pie-background:rgba(255,255,255,1);
border-style:dashed;
border-width:1px;
border-color:#3e9ce3;
position:absolute;
left:66px;
top:1852px;
z-index:71;
width:426px;
height:115px;
    behavior:url(/PIE.htc)}
</style>
<script src="./index_files/webfont.js"></script>
<script>
WebFont.load({
  google: {
    families: ['Lato','Lato']
  },
  active: function() {
   if(lp && lp.text && typeof lp.text.fixTextHeights === "function")  {
     lp.text.fixTextHeights();
   }
  }
});
</script>
<!--[if IE 7]>
<script>
(function() {
  var sheet = document.styleSheets[1];
  var rules = sheet.rules;
  var rule;
  var index = -1;
  for (var i=0, l=rules.length; i<l; i++){
    rule = rules[i];
    if (rule.selectorText.toLowerCase() == 'div.lp-pom-root .lp-pom-text span') {
      index = i;
      break;
    }
  }
  if (index > -1) {
    sheet.removeRule(index);
    sheet.addRule('div.lp-pom-root .lp-pom-text span', 'line-height:inherit');
  }
})();
</script>
<![endif]-->

<style type="text/css"></style><link type="text/css" rel="stylesheet" href="chrome-extension://cpngackimfmofbokmjmljamhdncknpmg/style.css"><script type="text/javascript" charset="utf-8" src="chrome-extension://cpngackimfmofbokmjmljamhdncknpmg/js/page_context.js"></script></head>
<body class="lp-pom-body " screen_capture_injected="true">
<script src="./index_files/iframe-api-v1.js"></script>
<!-- start Mixpanel --><script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f)}})(document,window.mixpanel||[]);
mixpanel.init("5cd765de35906b506851dd3455d5d4f7");</script><!-- end Mixpanel -->

<script>
  jQuery(document).ready(function(){
	setTimeout(function() {
		  var player = jQuery('#lp-pom-video-349 iframe')[0].wistiaApi;
			
		  player.bind("play", function() {
			mixpanel.track('video-played');
			return this.unbind;
		  });
	  }, 2500);
  });
  
  jQuery( function() {
    jQuery('#lp-pom-button-399').click(function() {
      mixpanel.track('landing-page-button');
      return true;
    });
    
    jQuery('#lp-pom-text-112 a').click(function() {
      mixpanel.track('landing-page-privacy');
      return true;
    });
    
  });
</script>
<div class="lp-element lp-pom-root" id="lp-pom-root">
<div class="lp-positioned-content">
<div class="lp-element lp-pom-text" id="lp-pom-text-33">
<p xmlns="">
<span style="font-family:lato;"><span style="color:#999999;"><span style="font-size: 13px; line-height: 17px;">© 2014 Proof Reading Monkey</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-38">
<div class="lp-element lp-pom-text" id="lp-pom-text-35">
<h2>
<span style="font-family:lato;"><span style="color:#585453;"><span style="font-size: 17px; line-height: 19px;">Rapid Turnaround</span></span></span>
</h2>
</div>
<div class="lp-element lp-pom-text" id="lp-pom-text-36">
<p>
<span style="font-family: lato;"><span style="color:#54514A;"><span style="font-size: 13px; line-height: 20px;">Unlike other services, we accept files 24/7 and have a turnaround time of just 12 hours.&nbsp;We work around the clock to guarantee you meet the deadline</span></span></span><span style="font-size: 13px; line-height: 20px; color: rgb(102, 102, 102); font-family: lato;">.</span>
</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-251">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/1riadhv-timeline.png" alt=""></div>
</div>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-54">
<div class="lp-element lp-pom-text" id="lp-pom-text-55">
<h2>
<span style="font-family:lato;"><span style="color:#585453;"><span style="font-size: 17px; line-height: 19px;">Thorough Review</span></span></span>
</h2>
</div>
<div class="lp-element lp-pom-text" id="lp-pom-text-56">
<p>
<span style="font-family:lato;"><span style="color:#54514A;"><span style="font-size: 13px; line-height: 20px;">Two professional proofreaders will thoroughly review your document, mark their changes&nbsp;</span></span><span style="color: rgb(102, 102, 102); font-size: 13px; line-height: 20px;">using Microsoft Word 'track changes' feature</span><span style="color: rgb(102, 102, 102); "><span style="font-size: 13px; line-height: 20px;">, and email it to you.&nbsp;</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-250">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/69z6qj-track-changes2.png" alt=""></div>
</div>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-59">
<div class="lp-element lp-pom-text" id="lp-pom-text-60">
<h2>
<span style="font-family:lato;"><span style="color:#585453;"><span style="font-size: 17px; line-height: 19px;">Satisfaction Guarantee</span></span></span>
</h2>
</div>
<div class="lp-element lp-pom-text" id="lp-pom-text-61">
<p>
<span style="font-family:lato;"><font color="#54514A" size="2"><span style="line-height: 20px;">All orders are subject to our 100% satisfaction guarantee.&nbsp;</span></font></span><span style="color: rgb(102, 102, 102); font-family: lato; font-size: small; line-height: 20px;">We will work until&nbsp;you&nbsp;find your document perfect. Or we'll refund your payment. That's how confident we are.</span>
</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-252">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/1w7xgs0-thumb-up.png" alt=""></div>
</div>
</div>
<div class="lp-element lp-pom-text" id="lp-pom-text-112">
<p style="text-align: right; ">
<span style="font-family:lato;"><a href="privacy-policy" style="text-decoration: none"><span style="color:#999999;"><span style="font-size: 13px; line-height: 17px;">Privacy Policy</span></span></a></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-224">
<p>
<span style="font-family:lato;"><strong style="text-decoration: none;"><span style="font-size:18px; text-decoration:none;"><span style="color: rgb(153, 153, 153);">Contact us:&nbsp;<a href="mailto:support@proofreadingmonkey.com" style="text-decoration:none;">support@proofreadingmonkey.com</a></span></span></strong></span>
</p>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-239"></div>
<div class="lp-element lp-pom-image" id="lp-pom-image-267">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/x1fqlz-logo-monkey.gif" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-268">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/1qisoa3-logo-text.png" alt=""></div>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-276">
<div class="lp-element lp-pom-image" id="lp-pom-image-270">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/ldqz3g-paypal.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-271">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/f2isyv-visa.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-272">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/4nlrz8-master-card.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-273">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/yx8k80-discover.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-274">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/c1pts1-dinersclub.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-275">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/1pim5y6-american-express.png" alt=""></div>
</div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-286">
<p>
<span style="color: rgb(234, 234, 234); font-size: 200px; line-height: 86px;">{</span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-287">
<p>
<span style="color: rgb(234, 234, 234); font-size: 200px; line-height: 86px;">}</span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-288">
<h2 class="lplh-38" style="text-align: center;">
<span style="font-size:36px;"><span style="font-family: arial, helvetica, sans-serif;"><span style="color: #3e9ce3;">‘’We find your errors before other people do.‘’</span></span></span>
</h2>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-289">
<p class="lplh-26" style="text-align: center;">
<span style="font-size:20px;"><span style="color: rgb(153, 153, 153); font-family: arial, helvetica, sans-serif; text-align: center;">You write it, we <em>right</em> it.</span></span>
</p>
</div>
<div class="lp-element lp-code" id="lp-code-334">
<h2 id="H2_1" style="margin: auto; margin-top: 20px; margin-bottom: 20px">
	We've helped over <b id="B_2">3214</b> people all around the world.
</h2>

<iframe width="100%" height="100%" src="./index_files/saved_resource.htm" id="IFRAME_350" style="border:none;">
          </iframe>
</div>
<div class="lp-element lp-pom-video" id="lp-pom-video-349">
<iframe src="http://fast.wistia.net/embed/iframe/ci22hjksvu?plugin%5BgoogleAnalytics%5D%5Bsrc%5D=%2F%2Ffast.wistia.com%2Flabs%2Fgoogle-analytics%2Fplugin.js&plugin%5BgoogleAnalytics%5D%5BoutsideIframe%5D=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="498" height="280"></iframe>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-358">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-178">
<p>
<span style="font-family:lato;"><span style="color:#54514A;"><span style="font-size: 13px; line-height: 20px;">Whether a Fortune 500 business report, academic text or a website copy, your document will be optimised for accuracy and readibilty of content.</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-179">
<h2>
<span style="font-family:lato;"><span style="color:#585453;"><span style="font-size: 17px; line-height: 19px;">Versatile Profile</span></span></span>
</h2>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-254">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/r0q2cb-document-check-compatibility.png" alt=""></div>
</div>
</div>
<div class="lp-element lp-code" id="lp-code-363">
<div id="DIV_1">
	<div class="container" id="DIV_2">
		<div class="testimonial" id="DIV_3">
			<div class="bubble" id="DIV_4">
				<p id="P_5">
					Thank you so much ! It was great.... I submitted the paper already ! Will definitely use proofreading monkey again in the future :) Please deliver my regards for the team. Aloha from Hawaii!
				</p><span id="SPAN_6"></span>
			</div>
			<div class="author" id="DIV_7">
				<img src="./index_files/i1PunyYGskBcn.jpg" id="IMG_8" alt="">
				<p id="P_9">
                  <strong>Priza Marendraputra</strong>,
				</p> <span id="SPAN_10">Research Assistant</span>
			</div>
		</div>
		<div class="testimonial" id="DIV_11">
			<div class="bubble" id="DIV_12">
				<p id="P_13">
					Proofreading Monkey has helped me with the edits to several VERY long papers, and the results have been error-free and very reasonably priced. I would recommend their services to any of my friends.
				</p><span id="SPAN_14"></span>
			</div>
			<div class="author" id="DIV_15">
				<img src="./index_files/iw0458fsqquyw.jpg" id="IMG_16" alt="">
				<p id="P_17">
                  <strong>John Hollins</strong>,
				</p> <span id="SPAN_18">Graduate Student in Political Science</span>
			</div>
		</div>
		<div class="testimonial last" id="DIV_19">
			<div class="bubble" id="DIV_20">
				<p id="P_21">
                    I have used Proofreading Monkey to help me with two of my e-books, and I was amazed to have my edits turned around within less than 24 hours. Great work!
				</p><span id="SPAN_22"></span>
			</div>
			<div class="author" id="DIV_23">
				<img src="./index_files/iIZdIBGNDExFH.jpg" id="IMG_24" alt="">
				<p id="P_25">
                  <strong>Steven Willwerth</strong>,
				</p> <span id="SPAN_26">Freelance Food Blogger</span>
			</div>
		</div>
		<div class="clearfix" id="DIV_27">
		</div>
	</div>
</div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-367">
<p>
<span style="color: #54514a; font-family: Lato, sans-serif; font-size: 60px; line-height: 35px; text-align: center;">what makes us different?</span>
</p>
<p>
	&nbsp;</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-370">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/8o4lez-contact-borders.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-371">
<p>
<span style="color:#6f6353;"><span style="font-family: Lato, sans-serif; font-size: 60px; line-height: 35px; text-align: center;">FAQ</span></span>
</p>
<p>
	&nbsp;</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-372">
<p>
<strong><span style="color:#6f6353;">What will I get in the end?</span></strong>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-373">
<p>
<strong><span style="color:#6f6353;">What does proofreading involve?</span></strong>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-374">
<p>
<strong><span style="color:#6f6353;">What file formats do you support?</span></strong>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-375">
<p>
<strong><span style="color:#6f6353;">Is all work done by real people?</span></strong>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-376">
<p>
<strong><span style="color:#6f6353;">Do you offer a money-back guarantee?</span></strong>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-377">
<p>
<strong><span style="color:#6f6353;">What types of payment do you accept?</span></strong>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-381">
<p>
<span style="color:#6f6353;">Our proofreading process involves making corrections to grammar, punctuation, spelling, use of appropriate words, internal consistency and logical structure.&nbsp;</span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-382">
<p>
<span style="color:#6f6353;">We accept PayPal, as well as Visa, MasterCard, American Express, Diners Club and Discover.</span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-383">
<p>
<span style="color:#6f6353;">Yes, and if you're not completely satisfied with our work, you can claim unlimited number of re-edits, free of charge.</span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-384">
<p>
<span style="color:#6f6353;">Yes, proofreading is done by skilled professionals who have been individually tested and verified by us. If you're looking for a great academic proofreader, chances are you'll find what you're looking for at ProofReadingMonkey.</span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-385">
<p>
<span style="color:#6f6353;">We handle most common file formats, such as Microsoft Word (.doc &amp; .docx). If you're under deadline pressure, please convert your file into one of the common file formats before sending it.</span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-386">
<p>
<span style="color:#6f6353;">You will receive a proofread document, that will look like this (see below).</span>
</p>
</div>
<a class="lp-element lp-pom-button" id="lp-pom-button-399" href="quote" target=""><span class="label" style="margin-top: -10px;">Get an Instant Price Quote</span></a>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-403">
<p class="lplh-77">
<H1 style="font-size:36px; color: rgb(84, 81, 74); font-family: Lato, sans-serif; text-align: center;">Professional editing &amp; proofreading services</H1>
</p>
<p>
	&nbsp;</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-404">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="./index_files/13lh12e-arrow-red-14_03f02n03f02n000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-405">
<p class="lplh-19" style="text-align: center; ">
<span style="font-family:lato;"><span style="font-size:14px;"><span style="color: rgb(153, 153, 153);">We charge as low as 0.01$ per word</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-410">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-411">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-453" style="border: 0px; background-color: rgba(255, 255, 255, 0); position: absolute; left: 14px; top: -13px; z-index: 72; width: 412px; height: 39px; outline: none !important;">
<p class="lplh-17" style="border: 0px; outline: none !important;">
<span style="font-size:11px;"><span style="border: 0px; outline: none !important;"><span style="border: 0px; outline: none !important; font-family: &#39;times new roman&#39;, times, serif;">I am&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">confiting</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">confiding</u></span>&nbsp;this&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">manuscripts</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">manuscript</u></span>&nbsp;to space<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);">-</span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">,&nbsp;not with the intention of saving myself, but</u></span>&nbsp;to help<span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">,</u></span>&nbsp;perhaps&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">inoverting</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">, to avert</u></span>&nbsp;the&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">apoling scurges which</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">appaling scourge that</u></span>&nbsp;is&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">minising</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">menacing</u></span>&nbsp;the human race<span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">.</u></span>&nbsp;Lord have pity on us<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">.</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">!</u></span></span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-456" style="border: 0px; background-color: rgba(255, 255, 255, 0); position: absolute; left: 14px; top: 47px; z-index: 73; width: 412px; height: 39px; outline: none !important;">
<p class="lplh-17" style="border: 0px; outline: none !important;">
<span style="font-size:11px;"><span style="border: 0px; outline: none !important;"><span style="border: 0px; outline: none !important; font-family: &#39;times new roman&#39;, times, serif;">As for me&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">Ulis Meru,</strike></span>&nbsp;<span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">Ulysse Merou,</u></span>&nbsp;I have set&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">of</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">off</u></span>&nbsp;again with my family in the&nbsp;<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);"><strike style="border: 0px; outline: none !important;">space ship we</strike></span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">spaceship. We</u></span>&nbsp;can keep going for several years<span style="border: 0px; outline: none !important; color: rgb(255, 0, 0);">-</span><span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">.</u></span>&nbsp;One day perhaps we shall come across a friendly planet<span style="border: 0px; outline: none !important; color: rgb(0, 0, 255);"><u style="border: 0px; outline: none !important;">.</u></span></span></span></span>
</p>
</div>
<p>
	&nbsp;</p>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-412"></div>
<div class="lp-element lp-pom-box" id="lp-pom-box-413"></div>
</div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-8" style="position: relative; width: 100%;">
<div class="lp-pom-block-content" style="margin: auto;"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-225" style="position: relative; width: null;">
<div class="lp-pom-block-content" style="margin: auto; width: 100%; height: 100%;"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-366" style="position: relative; width: 100%;">
<div class="lp-pom-block-content" style="margin: auto;"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-26" style="position: relative; width: 100%;">
<div class="lp-pom-block-content" style="margin: auto;"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-307" style="position: relative; width: null;">
<div class="lp-pom-block-content" style="margin: auto; width: 100%; height: 100%;"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-368" style="position: relative; width: 100%;">
<div class="lp-pom-block-content" style="margin: auto;"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-360" style="position: relative; width: 100%;">
<div class="lp-pom-block-content" style="margin: auto;"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-32" style="position: relative; width: 100%;">
<div class="lp-pom-block-content" style="margin: auto;"></div>
</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41124305-1', 'proofreadingmonkey.com');
  ga('send', 'pageview');

</script>
<script type="text/javascript" language="Javascript">
var LOCAL=false;var baseserverurl="http://www.uploadthingy.com";var loadimage="<img src=http://49video.resources.s3.amazonaws.com/loading.gif width=16 height=16>";var w_=new Array();w_["Your files are uploading"]="Your files are uploading";w_["via a secure SSL encrypted connection"]="via a secure SSL encrypted connection";w_["Please do not refresh or navigate away from this page until the upload is complete."]="Please do not refresh or navigate away from this page until the upload is complete.";w_["Uploading... "]="Uploading... ";w_["% complete"]="% complete";w_["remaining time"]="remaining time";w_[" minutes"]=" minutes";w_[" seconds"]=" seconds";w_["Ooops. The size of the file(s) you are uploading ("]="Ooops. The size of the file(s) you are uploading (";w_[") is larger than the maximum file size allowed "]=") is larger than the maximum file size allowed ";w_[". <a href=# onclick='authupload();'>Please click here to try again.</a>"]=". <a href=# onclick='authupload();'>Please click here to try again.</a>";w_["Ooops. Your upload has failed."]="Ooops. Your upload has failed. There must have been a problem with the connection somewhere between your computer and the upload server. <a href=# onclick='authupload();'>Please click here to try again.</a> <br><br>Server error: ";function replaceString(D,E){var C="";var B=0;while((il=D.indexOf("<$",B))!=-1){var F=D.indexOf("$>",B);var A=D.substring(il+2,F);if(E[A]){C+=D.substring(B,il)+E[A]}B=F+2}C+=D.substring(B);return C}function setHTML(B,A){if(!document.getElementById(B)){return}document.getElementById(B).innerHTML=A}function d(A){A=unescape(A);return A.replace(/\+/g," ")}function doBRs(A){A=A.replace(/\n/g,"");return A.replace(/<br>/g,"\n")}function $(A){return document.getElementById(A)}function cd(){return document.createElement("div")}function n(A){A=d(A);return A.replace(/ /g,"_")}function empty(A){if(!A){return}while(A.hasChildNodes()){A.removeChild(A.firstChild)}}function trim(A){return A.replace(/^\s+|\s+$/g,"")}var JSC_UPLOADSTATUS=0,JSC_FILELIST=1,JSC_GETSERVERUPLOAD=10,JSC_UPLOADPING=104;var currjson,currpw,curruploadpassword=".none.",currdomain,uploadmetadata,purchasedate,createdate,mmd,md;var sorteduploadlist,uploadcompletehtml=null,uploadcompleteredirect=null,uploadcompletefunction=null,savetozip=false,maxsize=null,showmaxsize=true,sendalertsto;var showblinks=true,trial=false;var oldrefcodes=new Array();var debugstatus="na";function handleJSON(command,json){currjson=json;if(json.s&&json.s=="badcode"){var m="Ooops. The sitecode supplied with this page will not work with this domain. Please make sure you entered the correct domain when you grabbed the code for this uploadthingy at uploadthingy.com.<br><br>Contact us at hello@uploadthingy.com if you have any questions. We'd love to help out!";setHTML("uploadarea",m);return}if(command==JSC_UPLOADSTATUS){if(json.status=="done"||json.error!=null){clearTimeout(uploadchecktimer);var m="";if(json.error){if(json.error=="server"){m="Ooops. Server error uploading file. <a href=# onclick='authupload();'>Please click here to try again</a>."}else{if(json.error=="authcode"){m="Ooops. Invalid upload authorization. <a href=# onclick='authupload();'>Please click here to try again</a>."}else{if(json.error=="video"){m="Ooops. That was not a valid file. Please try again."}else{if(json.error.indexOf("size")==0){var s1="",s2="";if(json.error.indexOf(";")!=-1){s1=json.error.substring(json.error.indexOf(";")+1,json.error.indexOf(">"))+"bytes";s2=json.error.substring(json.error.indexOf(">")+1)+"bytes"}m=w_["Ooops. The size of the file(s) you are uploading ("]+s1+w_[") is larger than the maximum file size allowed "]+(showmaxsize?s2:"")+w_[". <a href=# onclick='authupload();'>Please click here to try again.</a>"]}else{m=w_["Ooops. Your upload has failed."]+json.error}}}}setHTML("uploadarea",m)}else{var uploadrefcode=json.podcastid.substring(json.podcastid.lastIndexOf("$")+1);var nuc="/widget?c="+sitecode+"&a=newupload";if(uploadmetadata!=null){if(uploadmetadata.name!=null){nuc+="&name="+escape(uploadmetadata.name)}if(uploadmetadata.company!=null){nuc+="&company="+escape(uploadmetadata.company)}if(uploadmetadata.description!=null){nuc+="&description="+escape(uploadmetadata.description)}if(uploadmetadata.contact!=null){nuc+="&contact="+escape(uploadmetadata.contact)}if(uploadmetadata.sendtoemail!=null){nuc+="&sendtoemail="+escape(uploadmetadata.sendtoemail)}if(uploadmetadata.sendtotitle!=null){nuc+="&sendtotitle="+escape(uploadmetadata.sendtotitle)}if(savetozip){nuc+="&savetozip=yes"}if(sendalertsto){nuc+="&sendalertsto="+sendalertsto}for(i=0;i<30;i++){if(uploadmetadata["uploadfile"+i]!=null){var fn=uploadmetadata["uploadfile"+i];if(fn.lastIndexOf("\\")!=-1&&fn.lastIndexOf("\\")<fn.length-2){fn=fn.substring(fn.lastIndexOf("\\")+1)}if(fn.lastIndexOf("/")!=-1&&fn.lastIndexOf("/")<fn.length-2){fn=fn.substring(fn.lastIndexOf("/")+1)}nuc+="&uploadfile"+i+"="+escape(fn)}}nuc+="&refcode="+escape(uploadrefcode)+"&authcode="+uploadcode}else{uploadmetadata=new Array()}if(oldrefcodes[uploadrefcode]==null){sendCommand(baseserverurl,nuc);oldrefcodes[uploadrefcode]="sent"}uploadmetadata.refcode=uploadrefcode;var fcnt=0;if(savetozip){uploadmetadata["fhref"+fcnt]=baseserverurl+"/widget?c="+sitecode+"&el="+uploadrefcode+".zip";uploadmetadata["fname"+fcnt]=uploadrefcode+".zip"}else{for(i=0;i<30;i++){if(uploadmetadata["uploadfile"+i]!=null&&uploadmetadata["uploadfile"+i].length>0){var fn=uploadmetadata["uploadfile"+i];var ext="";var di=fn.lastIndexOf(".");if(di!=null){ext=fn.substring(di)}var dl=baseserverurl+"/widget?c="+sitecode+"&el="+uploadrefcode;if(i==0){dl+=ext}else{dl+="!"+i+ext}uploadmetadata["fhref"+fcnt]=dl;uploadmetadata["fname"+fcnt]=fn;fcnt++}}}var fl="";flcnt=0;while(uploadmetadata["fhref"+flcnt]){fl+="<br><a href="+uploadmetadata["fhref"+flcnt]+">"+uploadmetadata["fname"+flcnt]+"</a>";flcnt++}uploadmetadata.filelinks=fl;if(uploadcompleteredirect!=null){uploadcompleteredirect=replaceString(uploadcompleteredirect,uploadmetadata)}if(uploadcompletehtml!=null){m=replaceString(uploadcompletehtml,uploadmetadata)}else{if(uploadcompletefunction!=null){m=uploadcompletefunction()}else{if(uploadcompleteredirect!=null){m="<a href="+uploadcompleteredirect+">Redirecting</a>... "}else{m="Your upload has successfully completed. Use this code to reference your upload: <b>"+uploadrefcode+"</b>.";if(savetozip){m+="<br><br>Here's a link to a zip file with your uploaded files if you'd like to double check:"}else{if(fcnt>1){m+="<br><br>Here are links to your uploaded files if you'd like to double check:"}else{m+="<br><br>Here's a link to your uploaded file if you'd like to double check:"}}m+=uploadmetadata.filelinks;m+="<br><br><a href='javascript:authupload();'>Click here</a> to return to the upload form."}}}if(m){setHTML("uploadarea",m)}submitting=false}submitting=false}else{var info="";var remainingtime=0;if(json.status=="reading"){info+=json.percentcomplete+w_["% complete"];var elapsed=new Date().getTime()-uploadstart;if(eval(json.totalread)>0){remainingtime=elapsed/eval(json.totalread)*(eval(json.contentlength)-eval(json.totalread));remainingtime=remainingtime/1000;if(remainingtime>60){info+=", "+w_["remaining time"]+": "+Math.round(remainingtime/60)+w_[" minutes"]}else{info+=", "+w_["remaining time"]+": "+Math.round(remainingtime)+w_[" seconds"]}}}else{info+=json.status;pendingcnt++;if(pendingcnt>100){clearTimeout(uploadchecktimer);clearTimeout(uploadtimer);if(confirm("Ooops. It seems your upload has failed. Click 'Ok' to start over or 'Cancel' to continue this upload.")){window.location.reload()}pendingcnt=0}}clearTimeout(uploadtimer);var wait=1000;if(isNaN(uploadcnt)){alert("Ooops. It seems your upload has failed. Please contact us at hello@uploadthingy.com if this problem persists");return}if(uploadcnt>50&&remainingtime>20){wait=10000}else{if(uploadcnt>20){wait=5000}else{if(uploadcnt>5){wait=2000}}}debugstatus=json.status+"_"+uploadcnt+"_"+pendingcnt;uploadtimer=setTimeout("upload('go','"+info+"')",wait)}}else{if(command==JSC_GETSERVERUPLOAD){if(json.s=="ok"||json.s=="trialover"){if(!json.pd&&Date.parse(json.cd)>Date.parse("6/28/2008")&&Math.round((new Date().getTime()-Date.parse(json.cd))/(24*60*60*1000))>8){if(confirm("Ooops. Your trial period has ended or your subscription has been canceled.\n\nClick Ok to redirect to the subscription purchase page at uploadthingy.com. Click Cancel to remain on this page.")){if(window.parent){window.parent.location="http://www.uploadthingy.com/purchase.html"}else{document.location="http://www.uploadthingy.com/purchase.html"}}return}uploadcode=json.c;videoserverurl=json.u;if(videoserverurl.indexOf("https:")==0&&baseserverurl.indexOf("https:")==-1){baseserverurl="https"+baseserverurl.substring(4)}submitting=true;document.getElementById("uploadform").action=json.u+"/cire?uploadcode="+json.c+(maxsize==null?"":"&maxsize="+maxsize);if(document.getElementById("uploadid")){document.getElementById("uploadid").value="widget$"+json.d+"$"+json.c.substring(0,json.c.indexOf("_"))}else{document.getElementById("id").value="widget$"+json.d+"$"+json.c.substring(0,json.c.indexOf("_"))}document.getElementById("authcode").value=uploadcode;document.getElementById("uploadform").submit();uploadcnt=0;pendingcnt=0;uploadstart=new Date().getTime();uploadtimer=setTimeout("upload('go')",1000);uploadchecktimer=setTimeout("uploadCheck()",30000);uploadcntcheck=0}else{if(json.s=="notactive"){setHTML("uploadarea","Ooops. This uploadthingy has been deactivated.")}else{if(json.s=="noauth"){curruploadpassword=prompt("Please enter your upload password: ","");if(curruploadpassword!=null&&curruploadpassword.length>0){sendCommand(baseserverurl,"/widget?c="+sitecode+"&a=authupload&up="+curruploadpassword,null)}else{curruploadpassword=".none."}}else{if(confirm("Ooops. Could not connect to server. Click ok to try again.")){submitting=false;upload("start")}}}}}else{if(command==JSC_UPLOADPING){if(uploadcompleteredirect!=null){if(window.parent){window.parent.location=uploadcompleteredirect}else{document.location=uploadcompleteredirect}}}else{setHTML("uploadarea","Ooops. There was an error: "+json.s+", command="+command+". Please contact us at hello@uploadthingy.com.")}}}}var commandcnt=Math.ceil(Math.random()*10000000),ccbase=new Date().getTime()-1231877000000;var showwaitsave;function sendCommand(A,D,C){if(C){showwaitsave=document.getElementById(C).innerHTML;setHTML(C,"<p>"+loadimage+" &nbsp;Loading...</p>")}var B=document.createElement("script");B.src=A+D+"&cnt="+ccbase+"."+commandcnt;empty(document.getElementById("dataarea"));document.getElementById("dataarea").appendChild(B);commandcnt++}var uploadtimer,uploadcnt,uploadcode,videoserverurl,pendingcnt;var uploadchecktimer,uploadcntcheck;var uploadstart,submitting=false;function upload(C,B){if(C=="start"&&!submitting){sendCommand(baseserverurl,"/widget?c="+sitecode+"&a=authupload&up="+curruploadpassword,null);return false}else{++uploadcnt;var A="<p class=49widget_p>"+w_["Your files are uploading"];if(videoserverurl.indexOf("https:")==0){A+=" <b>"+w_["via a secure SSL encrypted connection"]+"</b>"}A+=". "+w_["Please do not refresh or navigate away from this page until the upload is complete."]+"</p>";A+="<p>"+loadimage+" &nbsp;"+w_["Uploading... "];if(B){A+=B}A+="</p>";setHTML("uploadarea",A);sendCommand(videoserverurl,"/jsc?uc="+uploadcode+"&debug="+encodeURIComponent(debugstatus),null)}return true}function authupload(){if(LOCAL){baseserverurl="http://localhost"}addIFrameToDOM("uploadtarget");addElementToDOM("span","dataarea");var A=new Array();A.c="xx_xx";A.d="x";var B="<a name=widget_top></a><form method='post' action='";B+="' enctype='multipart/form-data' id='uploadform' name='uploadform' target='uploadtarget' onsubmit=\"if (checkform()) {return upload('start');} else return false;\" >";B+=getUploadForm(A);B+="<input type=hidden name='suploadvideo' value='Upload'>";if(savetozip){B+="<input type=hidden name='savetozip' value='yes'>"}B+="</form><br>";setHTML("uploadarea",B)}var checkerrors=0;function uploadCheck(){if(isNaN(uploadcnt)){alert("Ooops. It seems your upload has failed. Please contact us at hello@uploadthingy.com if this problem persists");return}if(uploadcntcheck==uploadcnt){if(++checkerrors<5){upload("go","Reconnecting...")}else{if(confirm("Ooops. It seems your upload has failed. Click 'Ok' to start over or 'Cancel' to continue this upload.")){window.location.reload()}else{upload("go","Reconnecting...")}}}uploadcntcheck=uploadcnt;uploadchecktimer=setTimeout("uploadCheck()",30000)}function addIFrameToDOM(B){if(document.getElementById(B)){return}var A=document.getElementsByTagName("body").item(0);bgl=document.createElement("div");bgl.setAttribute("id","ut_ifc"+B);bgl.setAttribute("name","ut_ifc"+B);bgl.style.display="none";bgl.style.width=0;bgl.style.height=0;bgl.style.border=0;bgl.innerHTML="<iframe id='"+B+"' name='"+B+"' src='' style='width:0px;height:0px;border:0;'></iframe>";A.insertBefore(bgl,A.firstChild)}function addElementToDOM(C,D){if(!document.getElementById(D)){var A=document.getElementsByTagName("body").item(0);var B=document.createElement(C);B.setAttribute("id",D);B.setAttribute("name",D);B.style.display="none";B.style.width=0;B.style.height=0;B.style.border=0;A.insertBefore(B,A.firstChild)}};
</script>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0019/3569.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<!-- begin olark code -->
<script data-cfasync="false" type="text/javascript">/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7398-516-10-8723');/*]]>*/</script><noscript>&lt;a href="https://www.olark.com/site/7398-516-10-8723/contact" title="Contact us" target="_blank"&gt;Questions? Feedback?&lt;/a&gt; powered by &lt;a href="http://www.olark.com?welcome" title="Olark live chat software"&gt;Olark live chat software&lt;/a&gt;</noscript>
<!-- end olark code -->


</body></html>