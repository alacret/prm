<?php

/**
 * This file is responsible for counting words and storing files
 * Output format is JSON. Unique ID of a file will be returned.
 */
set_time_limit(90);

require('simple_html_dom.php');

define('JRE_PATH', dirname(__FILE__) . '/jre/bin');
define('UPLOAD_DIR', dirname(__FILE__) . '/uploads');

$allowedExtensions = array(
    'doc', 'docx', 'ppt', 'pttx',
    'rtf', 'txt',
    'odt', 'odp'
);

function exec_obfuscated($command, &$output = null, &$return_var = null) {
    $functionName = base64_decode('ZXhlYw=='); // exec
    return $functionName($command, $output, $return_var);
}

/**
 * Convert and count the words
 * @param type $inPath
 * @param type $outDir
 * @return type
 * @throws Exception
 */
function count_words_from_file($inPath) {
    $nameInfo = pathinfo($inPath);
    $extension = $nameInfo['extension'];
    if ($extension === "docx")
        return count_words_from_docx($inPath);
    elseif ($extension === "doc")
        return count_words_from_doc($inPath);

    $outDir = UPLOAD_DIR;
    $cmd = sprintf("env PATH=%s:%s CLASSPATH=%s libreoffice/opt/libreoffice4.1/program/soffice --headless --convert-to html --outdir %s %s", getenv('PATH'), JRE_PATH, JRE_PATH, escapeshellarg($outDir), escapeshellarg($inPath));
    exec_obfuscated($cmd, $output, $exitCode);
    // Absolutely no error reporting from CLI :(
    if (strpos(implode('', $output), "convert ") !== 0)
        throw new Exception('OpenOffice failed to convert');
    $pathInfo = pathinfo($inPath);
    $convertedFile = $pathInfo['dirname'] . '/' . $pathInfo['filename'] . '.html';
    $wordCount = count_words($convertedFile);

    unlink($convertedFile);

    return $wordCount;
}

function convert_docx_to_text($file) {

    $striped_content = '';
    $content = '';

    $zip = zip_open($file);

    if (!$zip || is_numeric($zip))
        return false;

    while ($zip_entry = zip_read($zip)) {

        if (zip_entry_open($zip, $zip_entry) == FALSE)
            continue;

        if (zip_entry_name($zip_entry) != "word/document.xml")
            continue;

        $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

        zip_entry_close($zip_entry);
    }// end while

    zip_close($zip);

    $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
    $content = str_replace('</w:r></w:p>', "\r\n", $content);
    $striped_content = strip_tags($content);

    return $striped_content;
}

function convert_doc_to_text($file) {
    $fileHandle = fopen($file, "r");
    $line = @fread($fileHandle, filesize($file));
    $lines = explode(chr(0x0D), $line);
    $outtext = "";
    foreach ($lines as $thisline) {
        $pos = strpos($thisline, chr(0x00));
        if (($pos !== FALSE) || (strlen($thisline) == 0)) {
            
        } else {
            $outtext .= $thisline . " ";
        }
    }
    return preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/", "", $outtext);
}

function count_words_from_docx($file) {
    return str_word_count(convert_docx_to_text($file));
}

function count_words_from_doc($file) {
    return str_word_count(convert_doc_to_text($file));
}

function generate_id($fileInfo) {
    return md5(rand() . microtime(true) . $fileInfo['tmp_name'] . $fileInfo['name']) . $fileInfo['name'];
}

function handle_upload($fileInfo, $uploadDir, $allowedExtensions) {
    if ($fileInfo['error'] != UPLOAD_ERR_OK)
        throw new Exception('Upload error != UPLOAD_ERROR_OK');

    $nameInfo = pathinfo($fileInfo['name']);
    $extension = $nameInfo['extension'];

    if (!in_array($extension, $allowedExtensions))
        throw new Exception('File extension not on the white list');

    $id = generate_id($fileInfo);
    $newPath = "$uploadDir/$id";

    move_uploaded_file($fileInfo['tmp_name'], $newPath);

    return $newPath;
}

function count_words($htmlFile) {
    $body = file_get_html($htmlFile)->find('body');

    if (count($body) == 0)
        return 0;

    $contents = $body[0]->plaintext;

    return str_word_count($contents, 0);
}

function process($fileInfo, $allowedExtensions) {
    try {
        //Moving the file
        $originalFile = handle_upload($fileInfo, UPLOAD_DIR, $allowedExtensions);

        $wordCount = count_words_from_file($originalFile);

        $pathInfo = pathinfo($originalFile);
        $name = $pathInfo['basename'];
        throw_success($name, $wordCount);
    } catch (Exception $e) {
        throw_error();
    }
}

function throw_error() {
    die(json_encode(array(
        'error' => true
    )));
}

function throw_success($fileName, $wordCount) {
    die(json_encode(array(
        'error' => false,
        'name' => $fileName,
        'words' => $wordCount
    )));
}

process($_FILES['file'], $allowedExtensions);
