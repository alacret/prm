<?php

class DB
{
	function __construct($dbname)
	{
		$dbExists = file_exists("$dbname.sqlite");
		$this->pdo = new PDO("sqlite:$dbname.sqlite");
		
		if(!$dbExists)
			$this->pdo->exec('CREATE TABLE IF NOT EXISTS orders (id INTEGER PRIMARY KEY, completed INT DEFAULT 0, `date` INT, email TEXT, file TEXT, words INT, total TEXT, time INT, payment TEXT)');
			
		$this->insertStmt = $this->pdo->prepare('INSERT INTO orders (completed,`date`,email,file,words,total,time,payment) VALUES(0,:date,:email,:file,:words,:total,:time,:payment)');
		$this->queryStmt = $this->pdo->prepare('SELECT * from orders WHERE completed = 0 OR date > :date ORDER BY completed ASC, date DESC');
		$this->completeStmt = $this->pdo->prepare('UPDATE orders SET completed = 1 WHERE id = :id');
	}

	function add($data)
	{
		$this->insertStmt->execute($data);
	}
	
	function get($date)
	{
		$this->queryStmt->execute(array(
			':date' => $date
		));

		return $this->queryStmt->fetchAll();
	}
	
	function complete($id)
	{
		$this->completeStmt->execute(array(
			':id' => $id
		));
	}
}


/*$model = new Model('orders');	

$model->add(array(
	':date' => strtotime('now'),
	':email' => 'test@test.test',
	':file' => 'uploads/tstfile.txt',
	':words' => '1337',
	':total' => '13.37',
	':time' => '24h',
	':payment' => 'Paypal'
));


$model->complete(2);

print_r($model->get(strtotime('-1 week')));*/