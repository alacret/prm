
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <!--9458c1e0-ed9d-11e3-aee5-22000a9a9c66 g-->
<title>Proofreading Service | Editing Service | Proofreadingmonkey.com</title>
<meta name="keywords" content="">
<meta name="description" content="">
<link href="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/reset-81c62fcc415bd2d6fa009d66c47174b6.css" media="screen" rel="stylesheet" type="text/css">
<link href="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/page_defaults-e9f5068d559c53318a0ec6a0d0012fce.css" media="screen" rel="stylesheet" type="text/css">
<meta name="robots" content="noindex, nofollow">
<style title="page-styles" type="text/css">
body { color:#677171; }
a {
  color:#f7941d;
  text-decoration:none;
}
#lp-pom-text-16 {
  left:0px;
  top:93px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  z-index:3;
  width:569px;
  height:237px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-204 {
  left:334px;
  top:2993px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  z-index:4;
  width:569px;
  height:152px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-206 {
  left:291px;
  top:2993px;
  display:block;
  z-index:5;
  position:absolute;
}
#lp-pom-image-209 {
  left:348px;
  top:3110px;
  display:block;
  z-index:6;
  position:absolute;
}
#lp-pom-text-233 {
  left:462px;
  top:3408px;
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  z-index:7;
  width:213px;
  height:14px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-root {
  display:block;
  background:rgba(238,238,238,1);
  -pie-background:rgba(238,238,238,1);
  margin:auto;
  padding-left:auto;
  padding-right:auto;
  padding-top:0px;
  min-width:1138px;
  behavior:url(/PIE.htc);
}
#lp-pom-block-8 {
  display:block;
  background:rgba(255,255,255,0.72);
  background-image:url(//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/shutterstock-photo-business-men-working-computer-darkened.original.jpg);
  background-repeat:no-repeat;
  background-position:center top;
  -pie-background:rgba(255,255,255,0.72) url(//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/shutterstock-photo-business-men-working-computer-darkened.original.jpg) no-repeat center top;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:655px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-text-395 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:435px;
  top:533px;
  z-index:20;
  width:266px;
  height:19px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-code-456 {
  display:block;
  left:823px;
  top:13px;
  z-index:17;
  width:315px;
  height:86px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-469 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none;
  left:387px;
  top:433px;
  z-index:1;
  width:364px;
  height:100px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-470 {
  display:block;
  left:375px;
  top:413px;
  z-index:21;
  position:absolute;
}
#lp-pom-text-471 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:135px;
  top:370px;
  z-index:19;
  width:300px;
  height:31px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-490 {
  display:block;
  left:561px;
  top:625px;
  z-index:25;
  position:absolute;
}
#lp-pom-text-491 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:524px;
  top:602px;
  z-index:26;
  width:90px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-492 {
  display:block;
  left:527px;
  top:602px;
  z-index:27;
  position:absolute;
}
#lp-pom-block-486 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:157px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-box-445 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:solid none none none;
  border-width:1px;
  border-color:#b9b9b9;
  left:0px;
  top:690px;
  z-index:22;
  width:1138px;
  height:26px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-box-494 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:none none dotted none;
  border-width:1px;
  border-color:#b9b9b9;
  left:131px;
  top:786px;
  z-index:28;
  width:876px;
  height:26px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-512 {
  display:block;
  left:527px;
  top:713px;
  z-index:41;
  position:absolute;
}
#lp-pom-image-513 {
  display:block;
  left:0px;
  top:713px;
  z-index:42;
  position:absolute;
}
#lp-pom-image-514 {
  display:block;
  left:715px;
  top:713px;
  z-index:43;
  position:absolute;
}
#lp-pom-image-515 {
  display:block;
  left:237px;
  top:717px;
  z-index:44;
  position:absolute;
}
#lp-pom-image-517 {
  display:block;
  left:366px;
  top:724px;
  z-index:45;
  position:absolute;
}
#lp-pom-button-31 {
  display:block;
  left:50px;
  top:25px;
  z-index:2;
  width:262px;
  height:48px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:5px;
  background-color:#f7941d;
  background:-webkit-linear-gradient(#f7941d,#d75305);
  background:-moz-linear-gradient(#f7941d,#d75305);
  background:-ms-linear-gradient(#f7941d,#d75305);
  background:-o-linear-gradient(#f7941d,#d75305);
  background:linear-gradient(#f7941d,#d75305);
  text-shadow:1px 1px #521601;
  -pie-background:linear-gradient(#f7941d,#d75305);
  color:#fff;
  border-style:solid;
  border-width:1px;
  border-color:#333333;
  font-size:16px;
  line-height:19px;
  font-weight:bold;
  font-family:Arial,Helvetica,sans-serif;
  text-align:center;
  background-repeat:no-repeat;
}
#lp-pom-block-511 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:25px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-block-280 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:none none none none;
  border-width:undefinedpx;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:428px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-text-282 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:-1px;
  top:1039px;
  z-index:8;
  width:330px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-397 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:0px;
  top:1074px;
  z-index:10;
  width:330px;
  height:145px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-400 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:404px;
  top:1074px;
  z-index:11;
  width:330px;
  height:116px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-401 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:404px;
  top:1039px;
  z-index:12;
  width:330px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-403 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:800px;
  top:1074px;
  z-index:13;
  width:330px;
  height:116px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-404 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:800px;
  top:1039px;
  z-index:14;
  width:330px;
  height:22px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-508 {
  display:block;
  left:79px;
  top:848px;
  z-index:38;
  position:absolute;
}
#lp-pom-image-509 {
  display:block;
  left:483px;
  top:841px;
  z-index:39;
  position:absolute;
}
#lp-pom-image-510 {
  display:block;
  left:880px;
  top:848px;
  z-index:40;
  position:absolute;
}
#lp-pom-block-156 {
  display:block;
  background:rgba(26,188,156,1);
  -pie-background:rgba(26,188,156,1);
  border-style:none none none none;
  border-width:undefinedpx;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:641px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-image-383 {
  display:block;
  left:195px;
  top:1413px;
  z-index:9;
  position:absolute;
}
#lp-pom-text-443 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:96px;
  top:1329px;
  z-index:16;
  width:945px;
  height:45px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-466 {
  display:block;
  left:297px;
  top:1439px;
  z-index:18;
  position:absolute;
}
#lp-pom-block-525 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:975px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-text-526 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:96px;
  top:1966px;
  z-index:48;
  width:945px;
  height:45px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-528 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:390px;
  top:2047px;
  z-index:49;
  width:699px;
  height:215px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-529 {
  display:block;
  left:334px;
  top:2038px;
  z-index:50;
  position:absolute;
}
#lp-pom-text-530 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:102px;
  top:2245px;
  z-index:51;
  width:141px;
  height:32px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-532 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:91px;
  top:2356px;
  z-index:52;
  width:699px;
  height:215px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-533 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:918px;
  top:2555px;
  z-index:53;
  width:141px;
  height:32px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-534 {
  display:block;
  left:42px;
  top:2350px;
  z-index:54;
  position:absolute;
}
#lp-pom-text-536 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:112px;
  top:2817px;
  z-index:55;
  width:121px;
  height:32px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-537 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:390px;
  top:2629px;
  z-index:56;
  width:699px;
  height:215px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-538 {
  display:block;
  left:341px;
  top:2624px;
  z-index:57;
  position:absolute;
}
#lp-pom-image-541 {
  display:block;
  left:73px;
  top:2038px;
  z-index:58;
  position:absolute;
}
#lp-pom-image-542 {
  display:block;
  left:889px;
  top:2350px;
  z-index:59;
  position:absolute;
}
#lp-pom-image-543 {
  display:block;
  left:73px;
  top:2612px;
  z-index:60;
  position:absolute;
}
#lp-pom-box-549 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:none none dotted none;
  border-width:1px;
  border-color:#b9b9b9;
  left:131px;
  top:2855px;
  z-index:63;
  width:876px;
  height:26px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-block-200 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  border-style:none none none none;
  border-width:undefinedpx;
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:395px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-text-442 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:469px;
  top:3159px;
  z-index:15;
  width:300px;
  height:44px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-548 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:96px;
  top:2920px;
  z-index:62;
  width:945px;
  height:45px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-554 {
  display:block;
  left:743px;
  top:3197px;
  z-index:65;
  position:absolute;
}
#lp-pom-block-547 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  margin-left:auto;
  margin-right:auto;
  margin-bottom:0px;
  width:100%;
  height:104px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-button-546 {
  display:block;
  left:437px;
  top:3276px;
  z-index:61;
  width:262px;
  height:48px;
  position:absolute;
  behavior:url(/PIE.htc);
  border-radius:5px;
  background-color:#f7941d;
  background:-webkit-linear-gradient(#f7941d,#d75305);
  background:-moz-linear-gradient(#f7941d,#d75305);
  background:-ms-linear-gradient(#f7941d,#d75305);
  background:-o-linear-gradient(#f7941d,#d75305);
  background:linear-gradient(#f7941d,#d75305);
  text-shadow:1px 1px #521601;
  -pie-background:linear-gradient(#f7941d,#d75305);
  color:#fff;
  border-style:solid;
  border-width:1px;
  border-color:#333333;
  font-size:16px;
  line-height:19px;
  font-weight:bold;
  font-family:Arial,Helvetica,sans-serif;
  text-align:center;
  background-repeat:no-repeat;
}
#lp-pom-block-232 {
  display:block;
  background:rgba(44,62,80,1);
  -pie-background:rgba(44,62,80,1);
  margin:auto;
  width:100%;
  height:69px;
  position:relative;
  behavior:url(/PIE.htc);
}
#lp-pom-box-497 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  border-style:none;
  left:699px;
  top:3391px;
  z-index:31;
  width:199px;
  height:48px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-505 {
  display:block;
  left:309px;
  top:3399px;
  z-index:30;
  position:absolute;
}
#lp-pom-text-523 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:9px;
  top:3405px;
  z-index:47;
  width:268px;
  height:19px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-524 {
  display:block;
  background:rgba(255,255,255,0);
  -pie-background:rgba(255,255,255,0);
  left:962px;
  top:3408px;
  z-index:46;
  width:123px;
  height:19px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-444 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  left:458px;
  top:-10px;
  z-index:23;
  width:229px;
  height:19px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-code-493 {
  display:block;
  left:0px;
  top:-32px;
  z-index:24;
  width:114px;
  height:27px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-text-495 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  left:458px;
  top:-10px;
  z-index:29;
  width:229px;
  height:0px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-image-498 {
  display:block;
  left:1px;
  top:11px;
  z-index:32;
  position:absolute;
}
#lp-pom-image-499 {
  display:block;
  left:33px;
  top:11px;
  z-index:33;
  position:absolute;
}
#lp-pom-image-500 {
  display:block;
  left:65px;
  top:11px;
  z-index:34;
  position:absolute;
}
#lp-pom-image-501 {
  display:block;
  left:97px;
  top:11px;
  z-index:35;
  position:absolute;
}
#lp-pom-image-502 {
  display:block;
  left:129px;
  top:11px;
  z-index:36;
  position:absolute;
}
#lp-pom-image-503 {
  display:block;
  left:161px;
  top:11px;
  z-index:37;
  position:absolute;
}
#lp-pom-text-550 {
  display:block;
  background:rgba(255,255,255,1);
  -pie-background:rgba(255,255,255,1);
  left:458px;
  top:-10px;
  z-index:64;
  width:229px;
  height:0px;
  position:absolute;
  behavior:url(/PIE.htc);
}
#lp-pom-root .lp-positioned-content {
  top:0px;
  width:1138px;
  margin-left:-569px;
}
#lp-pom-block-8 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:655px;
}
#lp-pom-block-486 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:157px;
}
#lp-pom-button-31:hover {
  background-color:#eb8016;
  background:-webkit-linear-gradient(#eb8016,#cc4504);
  background:-moz-linear-gradient(#eb8016,#cc4504);
  background:-ms-linear-gradient(#eb8016,#cc4504);
  background:-o-linear-gradient(#eb8016,#cc4504);
  background:linear-gradient(#eb8016,#cc4504);
  -pie-background:linear-gradient(#eb8016,#cc4504);
  color:#fff;
}
#lp-pom-button-31:active {
  background-color:#de7312;
  background:-webkit-linear-gradient(#d75305,#d75305);
  background:-moz-linear-gradient(#d75305,#d75305);
  background:-ms-linear-gradient(#d75305,#d75305);
  background:-o-linear-gradient(#d75305,#d75305);
  background:linear-gradient(#d75305,#d75305);
  -pie-background:linear-gradient(#d75305,#d75305);
  color:#fff;
}
#lp-pom-block-511 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:25px;
}
#lp-pom-block-280 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:428px;
}
#lp-pom-image-206 .lp-pom-image-container {
  width:31px;
  height:25px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-206 .lp-pom-image-container img {
  width:31px;
  height:25px;
}
#lp-pom-image-209 .lp-pom-image-container {
  width:93px;
  height:93px;
  behavior:url(/PIE.htc);
  border-radius:47px;
}
#lp-pom-image-209 .lp-pom-image-container img {
  width:93px;
  height:93px;
}
#lp-pom-block-156 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:641px;
}
#lp-pom-block-525 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:975px;
}
#lp-pom-block-200 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:395px;
}
#lp-pom-block-547 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:104px;
}
#lp-pom-block-232 .lp-pom-block-content {
  margin-left:auto;
  margin-right:auto;
  width:1138px;
  height:69px;
}
#lp-pom-image-383 .lp-pom-image-container {
  width:748px;
  height:440px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-383 .lp-pom-image-container img {
  width:748px;
  height:440px;
}
#lp-pom-image-466 .lp-pom-image-container {
  width:543px;
  height:357px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-466 .lp-pom-image-container img {
  width:543px;
  height:357px;
}
#lp-pom-image-470 .lp-pom-image-container {
  width:40px;
  height:66px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-470 .lp-pom-image-container img {
  width:40px;
  height:66px;
}
#lp-pom-image-490 .lp-pom-image-container {
  width:16px;
  height:13px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-490 .lp-pom-image-container img {
  width:16px;
  height:13px;
}
#lp-pom-image-492 .lp-pom-image-container {
  width:84px;
  height:36px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-492 .lp-pom-image-container img {
  width:84px;
  height:36px;
}
#lp-pom-image-498 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-498 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-499 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-499 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-500 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-500 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-501 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-501 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-502 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-502 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-503 .lp-pom-image-container {
  width:32px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-503 .lp-pom-image-container img {
  width:32px;
  height:32px;
}
#lp-pom-image-505 .lp-pom-image-container {
  width:122px;
  height:32px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-505 .lp-pom-image-container img {
  width:122px;
  height:32px;
}
#lp-pom-image-508 .lp-pom-image-container {
  width:170px;
  height:170px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-508 .lp-pom-image-container img {
  width:170px;
  height:170px;
}
#lp-pom-image-509 .lp-pom-image-container {
  width:171px;
  height:171px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-509 .lp-pom-image-container img {
  width:171px;
  height:171px;
}
#lp-pom-image-510 .lp-pom-image-container {
  width:170px;
  height:170px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-510 .lp-pom-image-container img {
  width:170px;
  height:170px;
}
#lp-pom-image-512 .lp-pom-image-container {
  width:156px;
  height:55px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-512 .lp-pom-image-container img {
  width:156px;
  height:55px;
}
#lp-pom-image-513 .lp-pom-image-container {
  width:223px;
  height:55px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-513 .lp-pom-image-container img {
  width:223px;
  height:55px;
}
#lp-pom-image-514 .lp-pom-image-container {
  width:414px;
  height:64px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-514 .lp-pom-image-container img {
  width:414px;
  height:64px;
}
#lp-pom-image-515 .lp-pom-image-container {
  width:111px;
  height:64px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-515 .lp-pom-image-container img {
  width:111px;
  height:64px;
}
#lp-pom-image-517 .lp-pom-image-container {
  width:120px;
  height:49px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-517 .lp-pom-image-container img {
  width:120px;
  height:49px;
}
#lp-pom-image-529 .lp-pom-image-container {
  width:31px;
  height:25px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-529 .lp-pom-image-container img {
  width:31px;
  height:25px;
}
#lp-pom-image-534 .lp-pom-image-container {
  width:31px;
  height:25px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-534 .lp-pom-image-container img {
  width:31px;
  height:25px;
}
#lp-pom-image-538 .lp-pom-image-container {
  width:31px;
  height:25px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-538 .lp-pom-image-container img {
  width:31px;
  height:25px;
}
#lp-pom-image-541 .lp-pom-image-container {
  width:200px;
  height:202px;
  behavior:url(/PIE.htc);
  border-radius:148px;
}
#lp-pom-image-541 .lp-pom-image-container img {
  width:200px;
  height:202px;
}
#lp-pom-image-542 .lp-pom-image-container {
  width:200px;
  height:200px;
  behavior:url(/PIE.htc);
  border-radius:148px;
}
#lp-pom-image-542 .lp-pom-image-container img {
  width:200px;
  height:200px;
}
#lp-pom-image-543 .lp-pom-image-container {
  width:200px;
  height:200px;
  behavior:url(/PIE.htc);
  border-radius:148px;
}
#lp-pom-image-543 .lp-pom-image-container img {
  width:200px;
  height:200px;
}
#lp-pom-button-546:hover {
  background-color:#eb8016;
  background:-webkit-linear-gradient(#eb8016,#cc4504);
  background:-moz-linear-gradient(#eb8016,#cc4504);
  background:-ms-linear-gradient(#eb8016,#cc4504);
  background:-o-linear-gradient(#eb8016,#cc4504);
  background:linear-gradient(#eb8016,#cc4504);
  -pie-background:linear-gradient(#eb8016,#cc4504);
  color:#fff;
}
#lp-pom-button-546:active {
  background-color:#de7312;
  background:-webkit-linear-gradient(#d75305,#d75305);
  background:-moz-linear-gradient(#d75305,#d75305);
  background:-ms-linear-gradient(#d75305,#d75305);
  background:-o-linear-gradient(#d75305,#d75305);
  background:linear-gradient(#d75305,#d75305);
  -pie-background:linear-gradient(#d75305,#d75305);
  color:#fff;
}
#lp-pom-image-554 .lp-pom-image-container {
  width:76px;
  height:101px;
  behavior:url(/PIE.htc);
}
#lp-pom-image-554 .lp-pom-image-container img {
  width:76px;
  height:101px;
}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="//d2xxq4ijfwetlm.cloudfront.net/m/lp-webapp/api/unbounce.js"></script><script type="text/javascript">
window.ub.page.id = "9458c1e0-ed9d-11e3-aee5-22000a9a9c66";
window.ub.page.variantId = "g";
window.ub.page.name = "Fork";
</script>

<script type="text/javascript">
var lp = lp || {};
lp.jQuery = jQuery.noConflict( true );
</script>

<script src="//d2xxq4ijfwetlm.cloudfront.net/m/lp-webapp/0.0.9/lp-text/2.9/main.js" type="text/javascript"></script>
<script src="//d2xxq4ijfwetlm.cloudfront.net/m/lp-webapp/0.0.9/lp-button/2.1/main.js" type="text/javascript"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
<style>

  /*Stylesheet by ConversionLab, hello@conversionlab.no*/
  
  /*h1 shadow and weight*/
  
  h1 {

    font-style:normal;
    font-weight:400;
    text-shadow:rgba(0,0,0,0.3) 0 0 5px;
    text-rendering: optimizelegibility;
  }
  
  /*h1 shadow and weight end*/
  
  h2,h3,h4,h5,h6{
    text-rendering: optimizelegibility;
    font-style:normal;
    text-shadow:rgba(0,0,0,0.3) 0 0 5px;
  }

 
  
  p, ul, div.lp-pom-root .lp-pom-text.nlh p, div.lp-pom-root .lp-pom-text.nlh ul {
    font-family:Lato, 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-style:normal;
    font-weight:normal;
    font-size:15px;
    line-height:23px;
    -webkit-font-smoothing: antialiased;
    text-rendering: optimizelegibility;
  }
  
  
/*button styles and hover effect end*/
  
/*full width image block*/
  
  #lp-pom-block-8 {  
    -webkit-background-size: cover !important;
    -moz-background-size: cover !important;   
    -o-background-size: cover !important;
    background-size: cover !important;
  }

  /*full width image block end*/

  /* transparent scroll arrow HAND */
  
  #lp-pom-image-483 { 
    cursor: pointer;
  }
  
  /* transparent scroll arrow HAND end */
  
    
</style>
<script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
lp = lp || {};
lp.webFontLoad = {
  google: {
    families: ['Lato']
  },
  active: function() {
   if(lp && lp.text && typeof lp.text.fixTextHeights === "function")  {
     lp.text.fixTextHeights();
   }
  }
};
WebFont.load(lp.webFontLoad);
</script>
<!--[if IE 7]>
<script>
(function() {
  var sheet = document.styleSheets[1];
  var rules = sheet.rules;
  var rule;
  var index = -1;
  for (var i=0, l=rules.length; i<l; i++){
    rule = rules[i];
    if (rule.selectorText.toLowerCase() == 'div.lp-pom-root .lp-pom-text span') {
      index = i;
      break;
    }
  }
  if (index > -1) {
    sheet.removeRule(index);
    sheet.addRule('div.lp-pom-root .lp-pom-text span', 'line-height:inherit');
  }
})();
</script>
<![endif]-->

</head>
<body class="lp-pom-body ">
<div class="lp-element lp-pom-root" id="lp-pom-root">
<div class="lp-positioned-content">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-16">
<h1 xmlns="" style="text-align: center;">
<font color="#ffffff" face="lato"><span style="font-size: 72px; line-height: 79px;">Professional Proofreading &amp; Editing Services.</span></font>
</h1>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-204">
<p class="lplh-38" style="text-align:center;">
<span style="color:#6F6F6F;"><span style="font-family:georgia,serif;"><span style="font-size:24px;"><em><span style="font-style: italic;">My Ph.D. advisor was speechless when I got my paper back from you guys. He said it was a very polished document. Please deliver my regards for the team. Aloha from Hawaii!&nbsp;</span></em></span></span></span>
</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-206">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/u9vmo2-quote-georgia_00v00p00v00p000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-209">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/ragr3i-henton-college-gradspotlight_04l04802l02l00v00d.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-233">
<p class="lplh-14" style="text-align: center;">
<span style="font-size:9px;"><span style="font-family:lato; letter-spacing: 0.25em;"><span style="color:#bdc3c7;">MADE WITH &hearts; IN NYC</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-282">
<p class="lplh-30" style="text-align: center;">
<span style="color:#252a2a;"><span style="font-family:lato;"><span style="font-size: 24px; ">Versatile Profile</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-383">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/1gzn7z4-frame-macbook_0ks0c80ks0c8000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-395">
<p class="lplh-19" style="text-align: center;">
<span style="font-family:lato;"><span style="color:#fff;">We charge </span><span style="color:#677171;"><span style="background-color:#feffb2;">as low as 0.01$ per word</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-397">
<p class="lplh-29 lplh-26" style="text-align: center;">
<span style="color:#677171;"><span style="font-size:16px;"><span style="font-family:lato;">Whether you&rsquo;re writing web copy, product descriptions, a thesis, novel, resume or anything in between, our<span style="background-color:#feffb2;"> native English-speaking editors</span> from the U.S., Canada, U.K., and Australia have the expertise to make it great. </span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-400">
<p class="lplh-29 lplh-26" style="text-align: center;">
<span style="color:#677171;"><span style="font-size:16px;"><span style="font-family:lato;">We cover <span style="background-color:#feffb2;">more than just basic proofreading</span>. We&rsquo;ll improve and provide feedback on grammar, spelling, punctuation, clarity, word choice, sentence structure, and idea flow. </span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-401">
<p class="lplh-30" style="text-align: center;">
<span style="color:#252a2a;"><span style="font-family:lato;"><span style="font-size: 24px; ">Thorough Review</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-403">
<p class="lplh-29 lplh-26" style="text-align: center;">
<span style="color:#677171;"><span style="font-size:16px;"><span style="font-family:lato;">We are open 24 hours a day, 7 days a week, and we offer <span style="background-color:#feffb2;">delivery as fast as 12 hours</span>. We work around the clock to guarantee you meet the deadline. </span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-404">
<p class="lplh-30" style="text-align: center;">
<span style="color:#252a2a;"><span style="font-family:lato;"><span style="font-size: 24px; ">Rapid Turnaround</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-442">
<p>
<span style="color:#6F6F6F;"><span style="font-family:lato;">Priza Marendraputra&nbsp;</span>
<br>
<span style="font-family:lato;">UC Berkeley, Research Assistant</span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-443">
<p class="lplh-45" style="text-align: center;">
<strong><span style="font-family:lato;"><span style="font-size:28px;"><span style="color:#ffffff;">Check Out an Example of a Paper We Have Improved</span></span></span></strong>
</p>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-445">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-444">
<p class="lplh-19" style="text-align: center;">
<span style="font-size:12px;"><span style="color:#b9b9b9;"><font face="lato">YOU'RE IN A GOOD COMPANY</font></span></span>
</p>
</div>
<div class="lp-element lp-code" id="lp-code-493">
<a name="section1"></a>

</div>
</div>
<div class="lp-element lp-code" id="lp-code-456">
<a href="https://twitter.com/prm_com" class="twitter-follow-button" data-show-count="true" data-size="large">Follow @prm_com</a>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-466">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/x8le1i-sampledoc1_0f309x0f309x000000.jpg" alt=""></div>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-469">
<a class="lp-element lp-pom-button" id="lp-pom-button-31" href="clkg/http/proofreadingmonkey.com/pricing" target="_self"><span class="label" style="margin-top: -10px;">OK - Get My Instant Price Quote</span></a>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-470">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/1ikifwf-arrow-white-right.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-471">
<h2 class="lplh-31">
<span style="font-size:26px;"><span style="color:#ffffff;"><span style="font-family:lato;">You write it, we <em>right</em> it.</span></span></span>
</h2>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-490">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/wyqsnc-scroll-arrow-60_00g00d00g00d000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-491">
<p style="text-align: center;">
<span style="color:#ffffff;"><span style="font-family:lato; opacity:0.6">Learn More&nbsp;</span></span>
</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-492">
<div class="lp-pom-image-container" style="overflow: hidden;">
<a href="#section1" target="_self"><img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/upycgw-84-36.png" alt=""></a>
</div>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-494">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-495"></div>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-497">
<div class="lp-element lp-pom-image" id="lp-pom-image-498">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/f2isyv-visa.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-499">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/4nlrz8-master-card.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-500">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/1pim5y6-american-express.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-501">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/yx8k80-discover.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-502">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/c1pts1-dinersclub.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-503">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/ldqz3g-paypal.png" alt=""></div>
</div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-505">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/fg8pb0-siteseal-sf-3-h-l-m.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-508">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/txwpek-documents_04q04q04q04q000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-509">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/erfp51-pencil_04r04r04r04r000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-510">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/dehut4-clock_04q04q04q04q000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-512">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/16h25a4-flashscore-com_04c01j04c01j000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-513">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/gubvwu-ambergames_06701j06701j000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-514">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/1jdi7zd-compass_0el01s0bi01s000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-515">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/zwlqes-ablv_03301s03301s000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-517">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/51ud7z-pins.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-523">
<p class="lplh-19" style="color: rgb(103, 113, 113); text-align: center;">
<span style="font-size:10px;"><span style="color: rgb(189, 195, 199); font-family: arial, helvetica, sans-serif;"><span style="text-decoration:none"><a href="mailto:support@proofreadingmonkey.com">SUPPORT@PROOFREADINGMONKEY.COM</a></span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-524">
<p class="lplh-19">
<span style="font-size:12px;"><span style="letter-spacing: 0.25em;"><span style="font-family:arial,helvetica,sans-serif;"><span style="color:#f7941d;">888-833-8385</span></span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-526">
<p class="lplh-45" style="text-align: center;">
<strong><span style="font-family:lato;"><span style="font-size:28px;"><span style="color:#1abc9c;">Meet Some of Our Editors</span></span></span></strong>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-528">
<p class="lplh-26">
<span style="font-size:16px;"><span style="font-family:georgia,serif;"><em>The English language is my life. I grew up in a really small town in Missouri with my nose in a book and a pen in my hand. I went on to earn an English Education degree at the University of Missouri. I have a <span style="background-color:#ffff99;">certificate to teach Secondary English, a TEFL certificate</span><span style="background-color:#ffffff;">, and a passion for helping students</span> become the best writers they can be. I have been studying and teaching English in America for several years; I also have years of ESL teaching and editing experience. So, whether you are a professional writer, a college student, or someone trying to learn English, I will work hard to make your writing great!</em></span></span>
</p>
<div>
    &nbsp;</div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-529">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/u9vmo2-quote-georgia_00v00p00v00p000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-530">
<p class="lplh-32">
<span style="font-size:20px;"><span style="color:#677171;"><span style="font-family:lato;">Jared Mendoza</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-532">
<p class="lplh-26">
<span style="font-size:16px;"><em><span style="font-family:georgia,serif;">I have nearly <span style="background-color:#ffff99;">fifteen years of editing experience, nineteen years of experience teaching English</span> at the middle, high school, and college level, and a background in marketing communications. I edit everything from manuals to resumes to very technical, graduate level theses. Some of my clients have included Helium, American Greetings, Marian Heath, students from the Lynch School of Education at Boston College, and Simply English. I am thrilled to now be a part of Team ProofreadingMonkey, a group of positively professional, seriously savvy, and utterly endearing editors!</span></em></span>
</p>
<div>
	&nbsp;</div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-533">
<p class="lplh-32">
<span style="font-size:20px;"><span style="color:#677171;"><span style="font-family:lato;">Natalie Herlihy</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-534">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/u9vmo2-quote-georgia_00v00p00v00p000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-536">
<p class="lplh-32">
<span style="font-size:20px;"><span style="color:#677171;"><span style="font-family:lato;">Daniel Facey</span></span></span>
</p>
</div>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-537">
<p class="lplh-26">
<span style="font-size:16px;"><em><span style="font-family:georgia,serif;">Are you writing a personal statement? A textbook chapter? Need your article or essay edited in a hurry? I'd be happy to look it over for you. As a <span style="background-color:#ffff99;">devoted professional editor with five years of experience</span>, I have seen it all. I pride myself on my working knowledge of a number of style guides (including <span style="background-color:#ffff99;">APA, Chicago, Harvard, and MLA</span>) and my base of satisfied clients. I have edited everything, including legal briefs and memos, scientific journal articles, fiction and nonfiction books, plays, theses, dissertations, application materials, marketing materials, and all types of written assignments.&nbsp;</span></em></span>
</p>
<div>
	&nbsp;</div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-538">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/u9vmo2-quote-georgia_00v00p00v00p000000.png" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-541">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/t13rt1-jared_05k05m05k05m000000.jpg" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-542">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/1xxdjmw-natalie_05k05k05k05k000000.jpg" alt=""></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-543">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/1i1wb3e-daniel_05k05k05k05k000000.jpg" alt=""></div>
</div>
<a class="lp-element lp-pom-button" id="lp-pom-button-546" href="clkg/http/proofreadingmonkey.com/pricing" target="_self"><span class="label" style="margin-top: -10px;">OK - Get My Instant Price Quote</span></a>
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-548">
<p class="lplh-45" style="text-align: center;">
<strong><span style="font-family:lato;"><span style="font-size:28px;"><span style="color:#1abc9c;">Here's What Our Customers Have to Say</span></span></span></strong>
</p>
</div>
<div class="lp-element lp-pom-box" id="lp-pom-box-549">
<div class="lp-element lp-pom-text nlh" id="lp-pom-text-550"></div>
</div>
<div class="lp-element lp-pom-image" id="lp-pom-image-554">
<div class="lp-pom-image-container" style="overflow: hidden;">
<img src="//d9hhrg4mnvzow.cloudfront.net/try.proofreadingmonkey.com/16zrgby-drawn-marker-red-arrow-original_02402t02402t000000.png" alt=""></div>
</div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-8">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-486">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-511">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-280">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-156">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-525">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-200">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-547">
<div class="lp-pom-block-content"></div>
</div>
<div class="lp-element lp-pom-block" id="lp-pom-block-232">
<div class="lp-pom-block-content"></div>
</div>
</div>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<script>
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0019/3569.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</body>
</html>
